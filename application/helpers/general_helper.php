<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists("menu_active")) {
	function menu_active($segment = 1) {
		$CI = &get_instance();

		return $CI->uri->segment($segment);
	}
}

if (!function_exists("rekam_medis_status")) {
	function rekam_medis_status($status) {
		switch ($status) {
			case 'A2':
				return "Diperiksa";
			case 'A3':
				return "Selesai";
			case 'A4':
				return "Dibatalkan";
			default:
				return "Dibuat";
		}
	}
}

if (!function_exists("convertBirtdateToAge")) {
	
	function convertBirtdateToAge($date) {
		$tanggal = new DateTime(date("Y-m-d", strtotime($date)));
		$today = new DateTime('today');
		return $today->diff($tanggal)->y." Tahun";
	}
}

if (!function_exists("populateJsonCheckbox")) {
	function populateJsonCheckbox($item, $json) {
		if (isset($json) && $json != 'null') {
			$array = json_decode($json);
			return in_array($item, $array);
		}
		return false;
	}
}