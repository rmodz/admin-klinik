<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('booking_model','booking',TRUE);
		$this->load->model('pelanggan_model','pelanggan',TRUE);
	}

	public function index()
	{
		$tbl_booking 	= $this->booking->listing();
		$tbl_pelanggan 	= $this->pelanggan->listing();
		// print_r($tbl_booking);die();
		$data = array (	'title'			=>	'Data Booking ('.count($tbl_booking).')',
						'tbl_booking'	=>	$tbl_booking,
						'tbl_pelanggan'	=>	$tbl_pelanggan,
						'judul'			=>	'Data tabel booking',
						'juduladd'		=>	'Form tambah data booking',
						'juduledit'		=>	'Form edit data booking',
						'juduldelete'	=>	'Form delete data booking',
						'isi'			=> 	'content/booking/list'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}
	
	public function tambah(){
		$tbl_booking 	= $this->booking->listing();
		// Validasi
		$valid = $this->form_validation;

		$valid->set_rules('id_pelanggan','id_pelanggan','required',
		array('required'			=>			'%s Harus Diisi'));	

		if ($valid->run() == true) {
		$i = $this->input;
		$data = array(	'id_pelanggan'		=>	$i->post('id_pelanggan'),
						'booking_date'		=>	$i->post('booking_date'),
						'created_at' 		=>  date('Y-m-d H:i:s'),
						'created_by' 		=>  $this->session->userdata('username')
						);
		// print_r($data);die();
		$this->booking->tambah($data);
		$this->session->set_flashdata('success', 'Data booking telah ditambah');
		redirect(base_url('Booking'),'refresh');
			}else{
		$this->session->set_flashdata('danger', 'Data booking telah digunakkan');
		redirect(base_url('Booking'),'refresh');
			}
	}

	public function delete($id = null){
	// Proteksi delete
	$this->check_login->check();
	if ($id) {
			// print_r($this->booking->delete($id)); die;
			$this->booking->delete($id);
			$this->session->set_flashdata('danger', 'Data booking berhasil di hapus', 'success');
		}
		redirect('Booking');
	}
}

/* End of file Booking.php */
/* Location: ./application/controllers/Booking.php */