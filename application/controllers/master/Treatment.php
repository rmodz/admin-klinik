<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Treatment extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('treatment_model','treatment',TRUE);
	}

	public function index()
	{
		$tbl_treatment = $this->treatment->listing();
		$data = array (	'title'			=>	'Data Treatment ('.count($tbl_treatment).')',
						'tbl_treatment'	=>	$tbl_treatment,
						'judul'			=>	'Data tabel treatment',
						'juduladd'		=>	'Form tambah data treatment',
						'juduledit'		=>	'Form edit data treatment',
						'juduldelete'	=>	'Form delete data treatment',
						'isi'			=> 	'content/treatment/list'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

	public function tambah(){
	
		$tbl_treatment = $this->treatment->listing();
		// Validasi
		$valid = $this->form_validation;
		
		$valid->set_rules('nama','nama','required|is_unique[tbl_treatment.nama]',
		array('required'			=>			'%s Harus Diisi',
				'is_unique'			=>			'%s <strong>'.$this->input->post('nama').'</strong> sudah ada'));	

		if ($valid->run() == true) {

		$i = $this->input;
		$data = array(	'nama'				=>	$i->post('nama'),
						'harga'				=>	$i->post('harga'),
						'created_at' 		=> date('Y-m-d H:i:s'),
						'created_by' 		=> $this->session->userdata('username')
						);
		$this->treatment->tambah($data);
		$this->session->set_flashdata('success', 'Data treatment telah ditambah');
		redirect(base_url('master/Treatment'),'refresh');
			}else{
		$this->session->set_flashdata('danger', 'Data treatment telah digunakkan');
		redirect(base_url('master/Treatment'),'refresh');
			}
	}

	public function edit($id){

	$tbl_treatment = $this->treatment->detail($id);
	// Validasi
	$valid = $this->form_validation;
	
	$valid->set_rules('nama','nama','required',
	array('required'			=>			'%s Harus Diisi'));	

	if ($valid->run() == TRUE) {
		$i = $this->input;
		$data = array(	'id'				=>	$id,
						'nama'				=>	$i->post('nama'),
						'harga'				=>	$i->post('harga'),
						'updated_at' 		=> date('Y-m-d H:i:s'),
						'updated_by' 		=> $this->session->userdata('username')
						);
		$this->treatment->edit($data);
		$this->session->set_flashdata('warning', 'Data treatment telah dirubah');
		redirect(base_url('master/Treatment'),'refresh');
		}
	}

	public function delete($id = null){
	// Proteksi delete
	$this->check_login->check();
	if ($id) {
			$this->treatment->delete($id);
			$this->session->set_flashdata('danger', 'Data treatment berhasil di hapus', 'success');
		}
		redirect('master/Treatment');
	}


}

/* End of file Produk.php */
/* Location: ./application/controllers/master/Produk.php */