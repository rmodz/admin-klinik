<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Riwayat_pelanggan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('pelanggan_model','pelanggan',TRUE);
	}

	public function list($id)
	{
		if (!$id) {
			redirect('master/Pelanggan','refresh');
		}

		$tbl_pelanggan 			= $this->pelanggan->detail_riwayat($id);
		
		$data = array (	'title'					=>	'Data Riwayat Pelanggan',
						'id_pelanggan'			=>	$id,
						'tbl_pelanggan'			=>	$tbl_pelanggan,
						'judul'					=>	'Data tabel riwayat pelanggan',
						'juduladd'				=>	'Form tambah riwayat pelanggan',
						'juduldelete'			=>	'Form delete riwayat pelanggan',
						'isi'					=> 	'content/riwayat/list'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

	public function tambah(){
			// Validasi
		$valid = $this->form_validation;
		
		$valid->set_rules('keterangan','keterangan','required',
		array('required'			=>			'%s Harus Diisi'));	

		if ($valid->run() == true) {

		$i = $this->input;
		$data = array(	'id_pelanggan'		=>	$i->post('id_pelanggan'),
						'keterangan'		=>	$i->post('keterangan')
						);
		$this->pelanggan->tambah_riwayat($data);
		$this->session->set_flashdata('success', 'Data pelanggan telah ditambah');
		redirect(base_url('master/Riwayat_pelanggan/list/'.$i->post('id_pelanggan')),'refresh');
			}
	}

	public function delete($id = null){
	// Proteksi delete
	$this->check_login->check();
	if ($id) {
			$this->pelanggan->delete_riwayat($id);
			$this->session->set_flashdata('danger', 'Data riwayat pelanggan berhasil di hapus', 'success');
		}
		redirect('master/Riwayat_pelanggan/list/'.$id);
	}
}

/* End of file Riwayat_pelanggan.php */
/* Location: ./application/controllers/master/Riwayat_pelanggan.php */