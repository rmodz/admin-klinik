<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk_model','produk',TRUE);
	}

	public function index()
	{
		$tbl_produk = $this->produk->listing();
		$data = array (	'title'			=>	'Data Produk ('.count($tbl_produk).')',
						'tbl_produk'	=>	$tbl_produk,
						'judul'			=>	'Data tabel produk',
						'juduladd'		=>	'Form tambah data produk',
						'juduledit'		=>	'Form edit data produk',
						'juduldelete'	=>	'Form delete data produk',
						'isi'			=> 	'content/produk/list'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

	public function tambah(){
	
		$tbl_produk = $this->produk->listing();
		// Validasi
		$valid = $this->form_validation;
		
		$valid->set_rules('nama','nama','required|is_unique[tbl_produk.nama]',
		array('required'			=>			'%s Harus Diisi',
				'is_unique'			=>			'%s <strong>'.$this->input->post('nama').'</strong> sudah ada'));	

		if ($valid->run() == true) {

		$i = $this->input;
		$data = array(	'nama'				=>	$i->post('nama'),
						'harga'				=>	$i->post('harga'),
						'created_at' 		=> date('Y-m-d H:i:s'),
						'created_by' 		=> $this->session->userdata('username')
						);
		$this->produk->tambah($data);
		$this->session->set_flashdata('success', 'Data produk telah ditambah');
		redirect(base_url('master/Produk'),'refresh');
			}else{
		$this->session->set_flashdata('danger', 'Data produk telah digunakkan');
		redirect(base_url('master/Produk'),'refresh');
			}
	}

	public function edit($id){

	$tbl_produk = $this->produk->detail($id);
	// Validasi
	$valid = $this->form_validation;
	
	$valid->set_rules('nama','nama','required',
	array('required'			=>			'%s Harus Diisi'));	

	if ($valid->run() == TRUE) {
		$i = $this->input;
		$data = array(	'id'				=>	$id,
						'nama'				=>	$i->post('nama'),
						'harga'				=>	$i->post('harga'),
						'updated_at' 		=> date('Y-m-d H:i:s'),
						'updated_by' 		=> $this->session->userdata('username')
						);
		$this->produk->edit($data);
		$this->session->set_flashdata('warning', 'Data produk telah dirubah');
		redirect(base_url('master/Produk'),'refresh');
		}
	}

	public function delete($id = null){
	// Proteksi delete
	$this->check_login->check();
	if ($id) {
			$this->produk->delete($id);
			$this->session->set_flashdata('danger', 'Data produk berhasil di hapus', 'success');
		}
		redirect('master/Produk');
	}


}

/* End of file Produk.php */
/* Location: ./application/controllers/master/Produk.php */