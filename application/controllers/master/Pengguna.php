<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$tbl_users = $this->users_model->listing();

		$data = array (	'title'			=>	'Data Pengguna ('.count($tbl_users).')',
						'tbl_users'		=>	$tbl_users,
						'judul'			=>	'Data tabel pengguna',
						'juduladd'		=>	'Form tambah data pengguna',
						'juduledit'		=>	'Form edit data pengguna',
						'juduldelete'	=>	'Form delete data pengguna',
						'isi'			=> 	'content/pengguna/list'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

	public function tambah(){
	
		$tbl_users = $this->users_model->listing();

		// Validasi
		$valid = $this->form_validation;
		
		$valid->set_rules('username','username','required|is_unique[tbl_users.username]',
		array('required'			=>			'%s Harus Diisi',
				'is_unique'			=>			'%s <strong>'.$this->input->post('username').'</strong> sudah ada'));	

		if ($valid->run() == true) {

		$i = $this->input;
		$data = array(	'nama'				=>	$i->post('nama'),
						'username'			=>	$i->post('username'),
						'password'			=>	sha1($i->post('password')),
						'role'				=>	$i->post('role'),
						'created_at' 		=> date('Y-m-d H:i:s'),
						'created_by' 		=> $this->session->userdata('username')
						);
		$this->users_model->tambah($data);
		$this->session->set_flashdata('success', 'Data pengguna telah ditambah');
		redirect(base_url('master/Pengguna'),'refresh');
			}
	}

	public function edit($id){

	$tbl_users = $this->users_model->detail($id);
	// Validasi
	$valid = $this->form_validation;
	
	$valid->set_rules('username','username','required',
	array('required'			=>			'%s Harus Diisi'));	

	if ($valid->run() == TRUE) {
		$i = $this->input;
		$data = array(	
						'id'				=>	$id,
						'nama'				=>	$i->post('nama'),
						'username'			=>	$i->post('username'),
						'password'			=>	sha1($i->post('password')),
						'role'				=>	$i->post('role')
						);
		$this->users_model->edit($data);
		$this->session->set_flashdata('warning', 'Data pengguna telah dirubah');
		redirect(base_url('master/Pengguna'),'refresh');
		}
	}

	public function delete($id = null){
	// Proteksi delete
	$this->check_login->check();
	if ($id) {
			$this->users_model->delete($id);
			$this->session->set_flashdata('danger', 'Data pengguna berhasil di hapus', 'success');
		}
		redirect('master/Pengguna');
	}
	
}

/* End of file Pengguna.php */
/* Location: ./application/controllers/pengguna/Pengguna.php */