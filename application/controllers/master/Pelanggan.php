<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('pelanggan_model','pelanggan',TRUE);
	}

	public function index()
	{
		$tbl_pelanggan = $this->pelanggan->listing();

		$data = array (	'title'			=>	'Data Pasien ('.count($tbl_pelanggan).')',
						'tbl_pelanggan'	=>	$tbl_pelanggan,
						'judul'			=>	'Data tabel pelanggan',
						'juduladd'		=>	'Form tambah data pelanggan',
						'juduledit'		=>	'Form edit data pelanggan',
						'juduldelete'	=>	'Form delete data pelanggan',
						'isi'			=> 	'content/pelanggan/list'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

	public function tambah(){
	
		$tbl_pelanggan = $this->pelanggan->listing();
		// Validasi
		$valid = $this->form_validation;
		
		$valid->set_rules('nama','nama','required|is_unique[tbl_pelanggan.nama]',
		array('required'			=>			'%s Harus Diisi',
				'is_unique'			=>			'%s <strong>'.$this->input->post('nama').'</strong> sudah ada'));	

		if ($valid->run() == true) {

		$i = $this->input;
		$data = array(	'nama'				=>	$i->post('nama'),
						'tanggal_lahir'		=>	$i->post('tanggal_lahir'),
						'alamat'			=>	$i->post('alamat'),
						'no_hp'				=>	$i->post('no_hp'),
						'pekerjaan'			=>	$i->post('pekerjaan'),
						'created_at' 		=> date('Y-m-d H:i:s'),
						'created_by' 		=> $this->session->userdata('username')
						);
		$this->pelanggan->tambah($data);
		$this->session->set_flashdata('success', 'Data pasien telah ditambah');
		redirect(base_url('master/Pelanggan'),'refresh');
			}
	}

	public function edit($id){

	$tbl_pelanggan = $this->pelanggan->detail($id);
	// Validasi
	$valid = $this->form_validation;
	
	$valid->set_rules('nama','nama','required',
	array('required'			=>			'%s Harus Diisi'));	

	if ($valid->run() == TRUE) {
		$i = $this->input;
		$data = array(	'id'				=>	$id,
						'nama'				=>	$i->post('nama'),
						'tanggal_lahir'		=>	$i->post('tanggal_lahir'),
						'alamat'			=>	$i->post('alamat'),
						'no_hp'				=>	$i->post('no_hp'),
						'pekerjaan'			=>	$i->post('pekerjaan'),
						'updated_at' 		=> date('Y-m-d H:i:s'),
						'updated_by' 		=> $this->session->userdata('username')
						);
		$this->pelanggan->edit($data);
		$this->session->set_flashdata('warning', 'Data pelanggan telah dirubah');
		redirect(base_url('master/Pelanggan'),'refresh');
		}
	}

	public function delete($id = null){
	// Proteksi delete
	$this->check_login->check();
	if ($id) {
			$this->pelanggan->delete($id);
			$this->session->set_flashdata('danger', 'Data pelanggan berhasil di hapus', 'success');
		}
		redirect('master/Pelanggan');
	}

}

/* End of file Pelanggan.php */
/* Location: ./application/controllers/master/Pelanggan.php */