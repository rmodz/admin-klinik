<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Transaksimodel','transaksi',TRUE);
		$this->load->model('pelanggan_model','pelanggan',TRUE);
	}

	public function index()
	{

		$tbl_transaksi 	= $this->transaksi->listing();
		$tbl_pelanggan 	= $this->pelanggan->listing();

		$data = array( 	'title'  			=> 'Halaman Dashborad',
						'tbl_transaksi'		=> $tbl_transaksi,
						'tbl_pelanggan'		=> $tbl_pelanggan,
					   	'isi' 				=> 'content/dashboard/list'
			 );

				$this->load->view('content/layout/wrapper',$data, FALSE);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */