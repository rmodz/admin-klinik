<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Transaksimodel','transaksi',TRUE);
		$this->load->model('Rekammedis_model','rekam_medis',TRUE);
		$this->load->model('treatment_model','treatment',TRUE);
		$this->load->model('produk_model','produk',TRUE);
	}

	public function index()
	{
		$tbl_transaksi 		= $this->transaksi->listing();
		$tbl_rekam_medis 	= $this->transaksi->listing_rekam_medis();
		$notis				= $this->transaksi->notis();

		$data = array (	'title'				=>	'Data Transaksi ('.count($tbl_transaksi).')',
						'tbl_transaksi'		=>	$tbl_transaksi,
						'tbl_rekam_medis'	=>	$tbl_rekam_medis,
						'notis'				=>	$notis,
						'judul'				=>	'Data tabel transaksi',
						'juduladd'			=>	'Form tambah data transaksi',
						'juduledit'			=>	'Form edit data transaksi',
						'juduldelete'		=>	'Form delete data transaksi',
						'isi'				=> 	'content/transaksi/list'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

	public function tambah() {
		// Pembuatan Nomor Otomatis
		$notis				= $this->transaksi->notis();

		$valid = $this->form_validation;
		
		$valid->set_rules	(	'id_rekam_medis', 'Data Pasien','required',
				array 	 	( 	'required'		=> '%s harus diisi'));

		if($valid->run() === TRUE){
			$now = new DateTime('today');
			$i = $this->input;

			$this->db->trans_start();
			// Ubah data rekam medis
			$hasil = $this->rekam_medis->edit([
				'id'					=>	$i->post('id_rekam_medis'),
				'status'				=>	'A3',
				'updated_at' 			=>  date('Y-m-d H:i:s'),
				'updated_by' 			=>  $this->session->userdata('username')
			]);

			// Tambah data transaksi
			$data = array 	(	
								'no_faktur'			=>	$notis,
								'id_rekam_medis'	=>	$i->post('id_rekam_medis'),
								'created_at' 		=>  date('Y-m-d H:i:s'),
								'created_by' 		=>  $this->session->userdata('username')
							);
			$id = $this->transaksi->tambah($data);

			// Tambah data transaksi detail dari rekam medis treatment | produk
			$tbl_rekam_treatment = $this->rekam_medis->listing_treatment($i->post('id_rekam_medis'));
			
			
			$tbl_rekam_produk = $this->rekam_medis->listing_produk($i->post('id_rekam_medis'));


			$detail_transaksi = [];
			foreach ($tbl_rekam_treatment as $key => $value) {
				array_push($detail_transaksi, [
					"id_transaksi" => $id,
					"id_ref" => $value->id_treatment,
					"type_ref" => null,
					"type" => "T1",
					"jumlah_barang" => 1,
					"total_harga" => $value->harga_treatment,
					"created_by" => $this->session->userdata('username'),
					'created_at' 		=>  date('Y-m-d H:i:s'),
				]);
			}

			foreach ($tbl_rekam_produk as $key => $value) {
				array_push($detail_transaksi, [
					"id_transaksi" => $id,
					"id_ref" => $value->id_produk,
					"type_ref" => $value->waktu,
					"type" => "T2",
					"jumlah_barang" => $value->jumlah,
					"total_harga" => $value->harga_produk * $value->jumlah,
					"created_by" => $this->session->userdata('username'),
					'created_at' 		=>  date('Y-m-d H:i:s'),
				]);
			}
			$this->transaksi->tambah_detail($detail_transaksi);
			$this->db->trans_complete();

			$this->session->set_flashdata('success', 'Data Telah Ditambah');
			redirect(base_url('Transaksi/edit/'.$id),'refresh');
		}
			redirect(base_url('Transaksi'),'refresh');
	}

	// Modul Crud Produk
	public function tambah_detail() {
		
		// print_r($this->input->post());die();

		$valid = $this->form_validation;
		
		$valid->set_rules	(	'id_transaksi', 'Data Pasien','required',
				array 	 	( 	'required'		=> '%s harus diisi'));

		$tbl_produk = $this->produk->detail($this->input->post('id_ref'));
		// print_r($tbl_produk);die();
		if($valid->run() === TRUE){
		$i = $this->input;
		$data = [
					"id_transaksi" 	=> $i->post("id_transaksi"),
					"id_ref" 		=> $i->post("id_ref"),
					"type" 			=> 'T2',
					"jumlah_barang" => $i->post("jumlah_barang"),
					"total_harga"	=> $tbl_produk->harga * $i->post("jumlah"),
					"created_by" 	=> $this->session->userdata('username'),
					"created_at" 	=> date("Y-m-d H:i:s")
				];
			$this->transaksi->tambah_detail_transaksi($data);
			$this->session->set_flashdata('success', 'Data Telah Ditambah');
			redirect(base_url('Transaksi/edit/'.$i->post("id_transaksi")),'refresh');
		}else{
			redirect(base_url('Transaksi/edit/'.$i->post("id_transaksi")),'refresh');
		}
	}

	public function edit($id)
	{
		$i = $this->input;

		if ($i->post()) {
			$data = $i->post();
			$data['jumlah_bayar'] = str_replace(".", "", substr($data['jumlah_bayar'], 4));
			$data['total_harga'] = str_replace(".", "", $data['total_harga']);
			if ($data['metode_pembayaran'] == "debit") {
				$data['jumlah_bayar'] = $data['total_harga'];
			}
			if ($data['jumlah_bayar'] < $data['total_harga']) {
				$this->session->set_flashdata('warning', 'Maaf, Pembayaran kurang');
				redirect(base_url('Transaksi/edit/'.$id),'refresh');
			}

			$edit = [
				"id" => $id,
				"selesai" => 1,
				"total_harga" => $data['total_harga'],
				"jumlah_pembayaran" => $data['jumlah_bayar'],
				"kembali" => intval($data['jumlah_bayar']) - intval($data['total_harga']),
				"metode_pembayaran" => $data['metode_pembayaran'] == "cash"? "P1":"P2",
				"nomor_kartu" => $data['nomor_kartu'],
				"updated_by" => $this->session->userdata('username'),
				"updated_at" => date("Y-m-d H:i:s")
			];
			// print_r($edit); die;
			// Update Status Transaksi
			$this->transaksi->edit($edit);

			$this->session->set_flashdata('success', 'Data Transaksi Berhasil disimpan');
			redirect(base_url('Transaksi'),'refresh');

		}else {
			$total_item 			= 0;
			$total_harga 			= 0;
			$tbl_transaksi 			= $this->transaksi->detail($id);
			$tbl_rekam_medis 		= $this->rekam_medis->detail($id);
			$tbl_rekam_treatment 	= $this->rekam_medis->listing_treatment($tbl_rekam_medis->id);
			$tbl_rekam_produk 		= $this->rekam_medis->listing_produk($tbl_rekam_medis->id);
			$tbl_treatment 			= $this->treatment->listing();
			$tbl_produk 			= $this->produk->listing();

			foreach ($tbl_transaksi->detail as $key => $value) {
				if ($value->type == "T2")
					$total_item += intval($value->jumlah_barang);
				$total_harga += intval($value->total_harga);
			}
			// print_r($tbl_transaksi);die;

			if ($tbl_transaksi) {
				$data = array 	('title'				=>	'Data Transaksi',
							 'tbl_transaksi'		=>	$tbl_transaksi,
							 'tbl_rekam_medis'		=>	$tbl_rekam_medis,
						 	 'tbl_rekam_treatment'	=>	$tbl_rekam_treatment,
						 	 'tbl_rekam_produk'		=>	$tbl_rekam_produk,
						 	 'tbl_treatment'		=>	$tbl_treatment,
						 	 'tbl_produk'			=>	$tbl_produk,
							 'total_item'			=>	$total_item,
							 'total_harga'			=>	$total_harga,
							 'isi'					=>	'content/transaksi/edit'
							);
				$this->load->view('content/layout/wrapper', $data, FALSE);
			}else {
				redirect(base_url('Transaksi'),'refresh');
			}
		}

	}

	public function delete_detail($id, $id_transaksi) {
		if ($id) {
			$this->transaksi->delete_detail($id);
		}
		redirect(base_url('Transaksi/edit/'.$id_transaksi),'refresh');
	}


	public function print($no_faktur)
	{
		$this->load->library('dompdf_gen');

		$tbl_transaksi_join = $this->transaksi->Join_detail_transaksi($no_faktur);
		$user = $this->session->userdata('username');
		// print_r($tbl_transaksi_join);die();

		$data = array 	( 	'tbl_transaksi_join'		=>	$tbl_transaksi_join,
							'user'						=>	$user
							);

		$this->load->view('content/report/report_transaksi', $data, FALSE);

		$paper_size = 'A4';
		// $orientation = 'landscape';
		$orientation = 'potrait';

		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("Struck_transaksi.pdf", array('Attachment' => 0 ));
	}

	public function delete($id = null){
	// Proteksi delete
	$this->check_login->check();
	if ($id) {
			$this->transaksi->delete($id);
			$this->session->set_flashdata('danger', 'Data transaksi berhasil di hapus', 'success');
		}
		redirect('Transaksi');
	}
}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */