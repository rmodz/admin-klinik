<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	// Load model
	public function __construct(){
		parent::__construct();
	}

	protected function checkLogin() {
		if($this->session->userdata('username') != "" && 
			$this->session->userdata('role') != "" )
			redirect('Dashboard','refresh');
	}

	public function index()
	{
		// Validasi
		$valid = $this->form_validation;

		$valid->set_rules('username','Username','required|trim|min_length[5]|max_length[32]',
				array ('required' 	=> '%s Harus Diisi',
				'min_length' 		=> '%s Minimal 5 Karakter',
				'max_length'		=> '%s Minimal 32 Karakter'));

		$valid->set_rules('password','Password','required|trim|min_length[6]',
				array ('required' 	=> '%s Harus Diisi',
				'min_length' 		=> '%s Minimal 6 Karakter'));

		if($valid->run()){
			$username 				= $this->input->post('username');
			$password 				= $this->input->post('password');
		// Compare dengan data didatabase
			$check_login			= $this->users_model->login($username,$password);
		// Kalo ada datanya yang cocok maka create session datanya ada 4 (id_user,username,nama,akses_level)
			if($check_login->num_rows() > 0){
				$check_login = $check_login->row();
				$this->session->set_userdata('id',$check_login->id);
				$this->session->set_userdata('username',$check_login->username);
				$this->session->set_userdata('nama',$check_login->nama);
				$this->session->set_userdata('role',$check_login->role);
				$this->session->set_flashdata('success','Anda berhasil login.....');
				redirect(base_url('Dashboard'),'refresh');
			}else{
				// Kalo datanya ga cocok maka muncul error dan redirect ke halam login
				$this->session->set_flashdata('danger', 'username atau password anda salah....');
				redirect(base_url('login'),'refresh');
			}	
		}
		// End validasi

		$this->checkLogin();
		$data = array('title' => 'Login Admin');
		$this->load->view('content/login/list', $data, FALSE);	
	}

	// Logout
	public function logout()
	{
		$this->check_login->logout();	
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */