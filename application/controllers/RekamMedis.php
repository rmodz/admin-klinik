<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RekamMedis extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Rekammedis_model','rekam_medis',TRUE);
		$this->load->model('Pelanggan_model','pelanggan',TRUE);
		$this->load->model('treatment_model','treatment',TRUE);
		$this->load->model('produk_model','produk',TRUE);
	}

	public function index()
	{
		$date = date('Y-m-d');
		if ($this->input->post('filter'))
			$date = $this->input->post('filter');

		$tbl_rekam_medis 	= $this->rekam_medis->listing($date);
		$tbl_pelanggan 		= $this->pelanggan->listing();

		// print_r($tbl_booking);die();
		$data = array (	'title'				=>	'Data Rekam Medis ('.count($tbl_rekam_medis).')',
						'tbl_rekam_medis'	=>	$tbl_rekam_medis,
						'tbl_pelanggan'		=>	$tbl_pelanggan,
						'date' => $date,
						'judul'				=>	'Data tabel rekam medis',
						'juduladd'			=>	'Form tambah data rekam medis',
						'juduledit'			=>	'Form edit data rekam medis',
						'juduldelete'		=>	'Form delete data rekam medis',
						'isi'				=> 	'content/rekam_medis/list'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

	public function tambah() {
		// print_r($this->input->post()); die;
		$i = $this->input;
		$data = [
			"id_pelanggan" => $i->post('id_pelanggan'),
			"tanggal_hadir" => date("Y-m-d"),
			"created_by" => $this->session->userdata('username'),
			"created_at" => date("Y-m-d H:i:s")
		];
		// print_r($data); die;
		$this->rekam_medis->tambah($data);
		$this->session->set_flashdata('sukses', 'Data Telah Ditambah');
		redirect(base_url('RekamMedis'),'refresh');	
	}

	public function edit($id)
	{
		$tbl_rekam_medis 		= $this->rekam_medis->detail($id);
		$tbl_rekam_treatment 	= $this->rekam_medis->listing_treatment($tbl_rekam_medis->id);
		$tbl_rekam_produk 		= $this->rekam_medis->listing_produk($tbl_rekam_medis->id);
		$tbl_treatment 			= $this->treatment->listing();
		$tbl_produk 			= $this->produk->listing();

		// print_r($tbl_rekam_medis);die();
		// print_r(populateJsonCheckbox("dahi", $tbl_rekam_medis->kerutan));die;
		$valid = $this->form_validation;
		
		$valid->set_rules	(	'type_kulit', 'Type Kulit','required',
				array 	 	( 	'required'		=> '%s harus diisi'));

		if($valid->run() === FALSE){
		// End Validasi
		$data = array 	('title'				=>	'Edit Data Rekam Medis',
						 'tbl_rekam_medis'		=>	$tbl_rekam_medis,
						 'tbl_rekam_treatment'	=>	$tbl_rekam_treatment,
						 'tbl_rekam_produk'		=>	$tbl_rekam_produk,
						 'tbl_treatment'		=>	$tbl_treatment,
						 'tbl_produk'			=>	$tbl_produk,
						 'isi'					=>	'content/rekam_medis/edit'
						);
		// print_r($data);die();
		$this->load->view('content/layout/wrapper', $data, FALSE);
		// Masuk Database
		}else{

			$i = $this->input;
			$data = array 	(	'id'					=>	$id,
								'jenis_kulit'			=>	$i->post('jenis_kulit'),
								'type_kulit'			=>	$i->post('type_kulit'),
								'kekenyalan'			=>	$i->post('kekenyalan'),
								'kelembapan'			=>	$i->post('kelembapan'),
								'glogaus'				=>	$i->post('glogaus'),
								'tonus_turgor'			=>	$i->post('tonus_turgor'),
								'diagnosis'				=>	$i->post('diagnosis'),
								'keluhan_utama'			=>	$i->post('keluhan_utama'),
								'keluhan_tambahan'		=>	$i->post('keluhan_tambahan'),
								'kerutan'				=>	json_encode($i->post('kerutan')),
								'kelainan_kulit'		=>	json_encode($i->post('kelainan_kulit')),
								'status'				=>	$i->post('submit')=='simpan' ? 'A1': 'A2',
								'updated_by'			=> $this->session->userdata('username'),
								'updated_at' 			=> date("Y-m-d H:i:s")
							);
			// print_r($data);die();
			$this->rekam_medis->edit($data);
			$this->session->set_flashdata('success', 'Data Telah Disimpan');
			if($i->post('submit')=='simpan') {
				redirect(base_url('RekamMedis/edit/'.$id),'refresh');
			}else {
				redirect(base_url('RekamMedis'),'refresh');
			}
		}
		// End Database
	}

	public function delete($id = null){
	// Proteksi delete
	$this->check_login->check();
	if ($id) {
			$tbl_rekam_medis 	= $this->rekam_medis->detail($id);
			if ($tbl_rekam_medis) {
				$this->rekam_medis->delete($id);
				$this->session->set_flashdata('danger', 'Data Telah Dihapus');
			}
			// $this->session->set_flashdata('danger', 'Data rekam medis berhasil di hapus', 'success');
		}
		redirect('RekamMedis');
	}


	public function view($id) {
		$tbl_rekam_medis 	= $this->rekam_medis->detail($id);
		$tbl_rekam_treatment = $this->rekam_medis->listing_treatment($tbl_rekam_medis->id);
		$tbl_rekam_produk = $this->rekam_medis->listing_produk($tbl_rekam_medis->id);
		$tbl_treatment 		= $this->treatment->listing();
		$tbl_produk 		= $this->produk->listing();

		$data = array 	('title'				=>	'Lihat Data Rekam Medis',
						 'tbl_rekam_medis'		=>	$tbl_rekam_medis,
						 'tbl_rekam_treatment'	=>	$tbl_rekam_treatment,
						 'tbl_rekam_produk'		=>	$tbl_rekam_produk,
						 'tbl_treatment'		=>	$tbl_treatment,
						 'tbl_produk'			=>	$tbl_produk,
						 'isi'					=>	'content/rekam_medis/view'
						);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

	// public function delete($id) {
	// 	$tbl_rekam_medis 	= $this->rekam_medis->detail($id);
	// 	if ($tbl_rekam_medis) {
	// 		$this->rekam_medis->delete($id);
	// 		$this->session->set_flashdata('success', 'Data Telah Dihapus');
	// 	}
	// 	redirect(base_url('RekamMedis'),'refresh');
	// }

	// Modul Crud Treatment
	public function tambah_treatment() {
		$i = $this->input;
		if (!empty($i->post('id_treatment'))) {
			$data = [
				"id_rekam_medis" => $i->post("id_rekam_medis"),
				"id_treatment" => $i->post("id_treatment"),
				"created_by" => $this->session->userdata('username'),
				"created_at" => date("Y-m-d H:i:s")
			];
			$this->rekam_medis->tambah_treatment($data);
			$this->session->set_flashdata('sukses', 'Data Telah Ditambah');
		}
		redirect(base_url('RekamMedis/edit/'.$i->post("id_rekam_medis")),'refresh');
	}

	public function delete_treatment($id, $id_rekam_medis) {
		if ($id) {
			$this->rekam_medis->hapus_treatment($id);
		}
		redirect(base_url('RekamMedis/edit/'.$id_rekam_medis),'refresh');
	}
	
	// Modul Crud Produk
	public function tambah_produk() {
		$i = $this->input;
		if (!empty($i->post('id_produk'))) {
			$data = [
				"id_rekam_medis" => $i->post("id_rekam_medis"),
				"id_produk" => $i->post("id_produk"),
				"waktu" => $i->post('waktu'),
				"jumlah" => $i->post('jumlah'),
				"created_by" => $this->session->userdata('username'),
				"created_at" => date("Y-m-d H:i:s")
			];
			$this->rekam_medis->tambah_produk($data);
			$this->session->set_flashdata('sukses', 'Data Telah Ditambah');
		}
		redirect(base_url('RekamMedis/edit/'.$i->post("id_rekam_medis")),'refresh');
	}

	public function ubah_produk() {
		$i = $this->input;
		if (!empty($i->post('id_produk'))) {
			$data = [
				"id" => $i->post("id"),
				"id_rekam_medis" => $i->post("id_rekam_medis"),
				"id_produk" => $i->post("id_produk"),
				"waktu" => $i->post('waktu'),
				"jumlah" => $i->post('jumlah'),
				"updated_by" => $this->session->userdata('username'),
				"updated_at" => date("Y-m-d H:i:s")
			];
			// print_r($data); die;
			$this->rekam_medis->ubah_produk($data);
			$this->session->set_flashdata('sukses', 'Data Telah Ditambah');
		}
		redirect(base_url('RekamMedis/edit/'.$i->post("id_rekam_medis")),'refresh');	
	}

	public function delete_produk($id, $id_rekam_medis) {
		if ($id) {
			$this->rekam_medis->hapus_produk($id);
		}
		redirect(base_url('RekamMedis/edit/'.$id_rekam_medis),'refresh');
	}

}

/* End of file RekamMedis.php */
/* Location: ./application/controllers/RekamMedis.php */