<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_transaksi extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Transaksimodel','transaksi',TRUE);
		$this->load->model('Rekammedis_model','rekam_medis',TRUE);
	}

	public function index()
	{
		$start = "";
		$end = "";
		if ($this->input->post('start') && $this->input->post('end')) {
			$start = $this->input->post('start');
			$end = $this->input->post('end');
			$tbl_transaksi 		= $this->transaksi->listing_report($start, $end);
		}else {
			$tbl_transaksi 		= $this->transaksi->listing();
		}
		
		$data = array (	'title'			=>	'Data Report Transaksi',
						'tbl_transaksi'	=>	$tbl_transaksi,
						'start_date' 	=> 	$start,
						'end_date' 		=> 	$end,
						'isi'			=> 	'content/transaksi/list_report'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

		public function print($start, $end)
	{
		$this->load->library('dompdf_gen');
		$tbl_transaksi 		= $this->transaksi->listing_report($start, $end);
		$user = $this->session->userdata('username');

		$data = [ 'tbl_transaksi' 		=> $tbl_transaksi,
				  'user' 				=> $user
				];

		// print_r($data);die();

		$this->load->view('content/report/report_transaksi_all', $data, FALSE);

		$paper_size = 'A4';
		// $orientation = 'landscape';
		$orientation = 'potrait';

		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("Laporan_pelanggan.pdf", array('Attachment' => 0 ));
	}

}

/* End of file Report_transaksi.php */
/* Location: ./application/controllers/report/Report_transaksi.php */