<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class report_produk extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk_model','produk',TRUE);
	}

	public function index()
	{
		$start 	= "";
		$end 	= "";
		if ($this->input->post('start') && $this->input->post('end')) {
			$start 	= $this->input->post('start');
			$end 	= $this->input->post('end');
			$tbl_produk 		= $this->produk->listing_report_produk($start, $end);
		}else {
			$tbl_produk 		= $this->produk->listing();
		}
		// print_r($tbl_booking);die();
		$data = array (	'title'			=>	'Data Report Produk',
						'tbl_produk'	=>	$tbl_produk,
						'start_date' 	=> $start,
						'end_date' 		=> $end,
						'isi'			=> 	'content/produk/list_report'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

	public function print($start, $end)
	{
		$this->load->library('dompdf_gen');

		$tbl_produk 		= $this->produk->listing_report_produk($start, $end);
		$user = $this->session->userdata('username');

		// print_r($tbl_produk);die();

		$data = array 	( 	'tbl_produk'		=>	$tbl_produk,
							'user'				=>	$user
							);

		$this->load->view('content/report/report_produk', $data, FALSE);

		$paper_size = 'A4';
		// $orientation = 'landscape';
		$orientation = 'potrait';

		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("Laporan_produk.pdf", array('Attachment' => 0 ));
	}

}

/* End of file report_pelanggan.php */
/* Location: ./application/controllers/report/report_pelanggan.php */