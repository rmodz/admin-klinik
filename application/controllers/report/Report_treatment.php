<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class report_treatment extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('treatment_model','treatment',TRUE);
	}

	public function index()
	{
		$start 	= "";
		$end 	= "";
		if ($this->input->post('start') && $this->input->post('end')) {
			$start 	= $this->input->post('start');
			$end 	= $this->input->post('end');
			$tbl_treatment 		= $this->treatment->listing_report_treatment($start, $end);
		}else {
			$tbl_treatment 		= $this->treatment->listing();
		}
		// print_r($tbl_booking);die();
		$data = array (	'title'			=>	'Data Report Treatment',
						'tbl_treatment'	=>	$tbl_treatment,
						'start_date' 	=> 	$start,
						'end_date' 		=> 	$end,
						'isi'			=> 	'content/treatment/list_report'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

	public function print($start, $end)
	{
		$this->load->library('dompdf_gen');

		$tbl_treatment 	= $this->treatment->listing_report_treatment($start, $end);
		$user = $this->session->userdata('username');

		$data = array 	( 	'tbl_treatment'		=>	$tbl_treatment,
							'user'				=>	$user
							);

		$this->load->view('content/report/report_treatment', $data, FALSE);

		$paper_size = 'A4';
		// $orientation = 'landscape';
		$orientation = 'potrait';

		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("Laporan_treatment.pdf", array('Attachment' => 0 ));
	}

}

/* End of file report_pelanggan.php */
/* Location: ./application/controllers/report/report_pelanggan.php */