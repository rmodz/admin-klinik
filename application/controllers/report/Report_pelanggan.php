<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class report_pelanggan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('pelanggan_model','pelanggan',TRUE);
	}

	public function index()
	{
		$start 	= "";
		$end 	= "";
		if ($this->input->post('start') && $this->input->post('end')) {
			$start 	= $this->input->post('start');
			$end 	= $this->input->post('end');
			$tbl_pelanggan 		= $this->pelanggan->listing_report_pelanggan($start, $end);
		}else {
			$tbl_pelanggan 		= $this->pelanggan->listing();
		}
		

		$data = array (	'title'			=>	'Data Report Pelanggan',
						'tbl_pelanggan'	=>	$tbl_pelanggan,
						'start_date' 	=> $start,
						'end_date' 		=> $end,
						'isi'			=> 	'content/pelanggan/list_report'
				);
		$this->load->view('content/layout/wrapper', $data, FALSE);
	}

		public function print($start, $end)
	{
		$this->load->library('dompdf_gen');

		$tbl_pelanggan = $this->pelanggan->listing_report_pelanggan($start, $end);
		$user = $this->session->userdata('username');

		// print_r($tbl_pelanggan);die();

		$data = array 	( 	'tbl_pelanggan'		=>	$tbl_pelanggan,
							'user'				=>	$user
							);

		$this->load->view('content/report/report_pelanggan', $data, FALSE);

		$paper_size = 'A4';
		// $orientation = 'landscape';
		$orientation = 'potrait';

		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("Laporan_pelanggan.pdf", array('Attachment' => 0 ));
	}

}

/* End of file report_pelanggan.php */
/* Location: ./application/controllers/report/report_pelanggan.php */