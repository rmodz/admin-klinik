<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksimodel extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function notis() {
    	$current_date = date('Yd');
    	$notis = $current_date.str_pad(1, 4, "0", STR_PAD_LEFT);
    	$this->db->like('no_faktur',$current_date);
    	$this->db->order_by('created_at', 'desc');
    	$tbl_transaksi = $this->db->get('tbl_transaksi')->row();
    	if ($tbl_transaksi) {
    		$notis = intval($tbl_transaksi->no_faktur) + 1;
    	} 
    	return $notis;
    }

		// Listing
	public function listing()
	{
		$this->db->order_by('created_at', 'DESC');
		$query = $this->db->get_where('tbl_transaksi', ['deleted_at' => NULL]);
		return $query->result();
	}

	public function listing_report($start, $end) {
		$this->db->order_by('created_at', 'DESC');
		$this->db->where('created_at BETWEEN "'. date('Y-m-d', strtotime($start)) .' 00:00:00" AND "'. date('Y-m-d', strtotime($end)) .' 23:59:00"', null, false);
		$query = $this->db->get_where('tbl_transaksi', ['deleted_at' => NULL, 'selesai' => 1]);
		return $query->result();
	}

	public function listing_join_rekam_medis()
	{
		$this->db->select("t1.*, t2.no_faktur, t2.total_harga, t2.jumlah_pembayaran, t2.metode_pembayaran, t2.nomor_kartu,t2.selesai,t3.nama");
		$this->db->join('tbl_transaksi as t2','t2.id_rekam_medis=t1.id','LEFT');
		$this->db->join('tbl_pelanggan as t3','t1.id_pelanggan=t3.id','LEFT');
		$query = $this->db->get_where('tbl_rekam_medis as t1', ['t2.deleted_at' => NULL]);
		return $query->result();
	}

	// Listing rekam medis
	public function listing_rekam_medis()
	{
		$this->db->select("t1.*, t2.nama as nama_pelanggan");
		$this->db->join('tbl_pelanggan as t2','t1.id_pelanggan=t2.id','LEFT');
		$query = $this->db->get_where('tbl_rekam_medis as t1', ['t1.deleted_at' => NULL,'t1.status' => 'A2','t1.tanggal_hadir' => date('Y-m-d')]);
		return $query->result();
	}

	// Detail Transaksi
	public function detail_transaksi($id)
	{
		$q = $this->db->get_where('tbl_transaksi_detail', ['deleted_at' => NULL, 'id_transaksi' => $id])->result();
		foreach ($q as $key => $value) {
			if ($value->type == "T1") {
				$treatment = $this->db->get_where('tbl_treatment', ['deleted_at' => null, 'id' => $value->id_ref])->row();
				$q[$key]->nama = $treatment->nama;
				$q[$key]->harga = $treatment->harga;
			}else {
				$produk = $this->db->get_where('tbl_produk', ['deleted_at' => null, 'id' => $value->id_ref])->row();
				$q[$key]->nama = $produk->nama;
				$q[$key]->harga = $produk->harga;
			}
		}
		return $q;
	}

	// Detail 
	public function detail($id)
	{
		$this->db->select('tbl_transaksi.*, tbl_pelanggan.nama as nama_pelanggan, tbl_pelanggan.tanggal_lahir as tanggal_lahir_pelanggan');
		$this->db->join('tbl_rekam_medis', 'tbl_transaksi.id_rekam_medis = tbl_rekam_medis.id', 'LEFT');
		$this->db->join('tbl_pelanggan', 'tbl_rekam_medis.id_pelanggan = tbl_pelanggan.id', 'LEFT');
		$query = $this->db->get_where('tbl_transaksi', ['tbl_transaksi.deleted_at' => NULL, 'tbl_transaksi.id' => $id]);
		$detail = $query->row();
		if ($detail) {
			$detail->detail = $this->detail_transaksi($detail->id);
		}
		return $detail;
	}

	// Tambah / Insert data 
	public function tambah($data)
	{
		$this->db->insert('tbl_transaksi',$data);
		// Buat ambil data id autoincrement
		return $this->db->insert_id();
	}

	// Tambah / Insert data 
	public function tambah_detail_transaksi($data)
	{
		$this->db->insert('tbl_transaksi_detail',$data);
		return $this->db->insert_id();
	}

	public function tambah_detail($data)
	{
		// untuk insert banyak data kedalam tabel
		return $this->db->insert_batch('tbl_transaksi_detail',$data);
	}

	public function tambah_bayar($data)
	{
		$this->db->insert('tbl_transaksi_bayar',$data);
	}

	public function delete_detail($id =null)
	{
		$this->db->where('id',$id);
		$this->db->update('tbl_transaksi_detail',['deleted_at' => date("Y-m-d h:i:sa")]);
	}

	// Edit / Update 
	public function edit($data)
	{
		$this->db->where('id',$data['id']);
		$this->db->update('tbl_transaksi',$data);
	}

	// Delete
	public function delete($id =null)
	{
		$this->db->where('id',$id);
		$this->db->update('tbl_transaksi',['deleted_at' => date("Y-m-d h:i:sa")]);
	}

	public function check_transaksi_lama($id_rekam_medis)  {
		$query = $this->db->get_where('tbl_transaksi', ['deleted_at' => NULL, 'id_rekam_medis' => $id_rekam_medis, 'selesai' => 0]);
		return $query->num_rows();
	}

	public function Join_detail_transaksi($no_faktur)
	{
		$query = $this->db->get_where('tbl_transaksi', ['tbl_transaksi.deleted_at' => NULL, 'tbl_transaksi.no_faktur' => $no_faktur, 'tbl_transaksi.selesai' => 1]);
		$detail = $query->row();

		if ($detail) {
			$detail->detail = $this->detail_transaksi($detail->id);
		}
		return $detail;
	}

}

/* End of file Transaksimodel.php */
/* Location: ./application/models/Transaksimodel.php */