<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan_Model extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
		// Listing
	public function listing()
	{
		$query = $this->db->get_where('tbl_pelanggan', ['deleted_at' => NULL]);
		return $query->result();
	}

	public function listing_report_pelanggan($start, $end) {
		$this->db->join('tbl_rekam_medis', 'tbl_transaksi.id_rekam_medis = tbl_rekam_medis.id', 'LEFT');
		$this->db->join('tbl_pelanggan', 'tbl_rekam_medis.id_pelanggan = tbl_pelanggan.id', 'LEFT');
		$this->db->order_by('tbl_transaksi.created_at', 'DESC');
		$this->db->where('tbl_transaksi.created_at BETWEEN "'. date('Y-m-d', strtotime($start)) .' 00:00:00" AND "'. date('Y-m-d', strtotime($end)) .' 23:59:00"', null, false);
		$query = $this->db->get_where('tbl_transaksi', ['tbl_transaksi.deleted_at' => NULL]);
		return $query->result();
	}

		// Detail 
	public function detail($id)
	{
		$query = $this->db->get_where('tbl_pelanggan', ['deleted_at' => NULL, 'id' => $id]);
		return $query->row();
	}

	// Tambah / Insert data 
	public function tambah($data)
	{
		$this->db->Insert('tbl_pelanggan',$data);
	}

	// Edit / Update 
	public function edit($data)
	{
		$this->db->where('id',$data['id']);
		$this->db->update('tbl_pelanggan',$data);
	}

	// Delete
	public function delete($id =null)
	{
		$this->db->where('id',$id);
		$this->db->update('tbl_pelanggan',['deleted_at' => date("Y-m-d h:i:sa")]);
	}

	public function detail_riwayat($id)
	{
		$query = $this->db->get_where('tbl_riwayat_pelanggan', ['deleted_at' => NULL, 'id_pelanggan' => $id]);
		return $query->result();
	}

	// Tambah / Insert data 
	public function tambah_riwayat($data)
	{
		$this->db->insert('tbl_riwayat_pelanggan',$data);
	}

	// Delete
	public function delete_riwayat($id =null)
	{
		$this->db->where('id',$id);
		$this->db->update('tbl_riwayat_pelanggan',['deleted_at' => date("Y-m-d h:i:sa")]);
	}

}

/* End of file Pelanggan_Model.php */
/* Location: ./application/models/Pelanggan_Model.php */