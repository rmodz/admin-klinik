<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
		// Listing user
	public function listing()
	{
		$query = $this->db->get_where('tbl_users', ['deleted_at' => NULL]);
		return $query->result();
	}

		// Detail user
	public function detail($id)
	{
		$query = $this->db->get_where('tbl_users', ['deleted_at' => NULL, 'id' => $id]);
		return $query->row();
	}

		// Login user
	public function login($username,$password)
	{
		$this->db->select('*');
		$this->db->from('tbl_users');
		$this->db->where(array('username'			=>	$username,
								'password'			=>	sha1($password)));
		$query = $this->db->get();
		return $query;
	}

	// Tambah / Insert data user
	public function tambah($data)
	{
		$this->db->Insert('tbl_users',$data);
	}

	// Edit / Update user
	public function edit($data)
	{
		$this->db->where('id',$data['id']);
		$this->db->update('tbl_users',$data);
	}

	// Edit / Delete user
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->update('tbl_users',['deleted_at' => date("Y-m-d h:i:sa")]);
	}

}

/* End of file Users_Model.php */
/* Location: ./application/models/Users_Model.php */