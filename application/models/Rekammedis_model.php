<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekammedis_model extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
		// Listing
	public function listing($date)
	{
		$this->db->select('tbl_rekam_medis.*,tbl_pelanggan.nama,tbl_pelanggan.no_hp');
		$this->db->from('tbl_rekam_medis');
		$this->db->join('tbl_pelanggan','tbl_rekam_medis.id_pelanggan=tbl_pelanggan.id','LEFT');
		$this->db->where('tbl_rekam_medis.tanggal_hadir', $date);
		$this->db->where('tbl_rekam_medis.deleted_at', NULL);
		$this->db->order_by('tbl_rekam_medis.id');
		$query = $this->db->get();
		return $query->result();
	}

		// Detail
	public function detail($id)
	{
		$this->db->select('tbl_rekam_medis.*,tbl_pelanggan.nama,tbl_pelanggan.tanggal_lahir,tbl_pelanggan.no_hp,tbl_pelanggan.pekerjaan,tbl_pelanggan.alamat');
		$this->db->from('tbl_rekam_medis');
		$this->db->join('tbl_pelanggan','tbl_rekam_medis.id_pelanggan=tbl_pelanggan.id','LEFT');
		$this->db->where('tbl_rekam_medis.deleted_at', NULL);
		$this->db->where('tbl_rekam_medis.id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	// Tambah
	public function tambah($data)
	{
		$this->db->insert('tbl_rekam_medis',$data);
	}

	// Edit
	public function edit($data)
	{
		$this->db->where('id',$data['id']);
		$this->db->update('tbl_rekam_medis',$data);
	}

	// Delete
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->update('tbl_rekam_medis',['deleted_at' => date("Y-m-d h:i:s")]);
	}		

	// Listing Rekam Treatment
	public function listing_treatment($id_rekam_medis) {
		$this->db->select("t1.*, t2.nama as nama_treatment, t2.harga as harga_treatment");
		$this->db->join('tbl_treatment as t2','t1.id_treatment=t2.id','LEFT');
		$query = $this->db->get_where('tbl_rekam_treatment as t1', ['t1.id_rekam_medis' => $id_rekam_medis ,'t1.deleted_at' => NULL]);
		return $query->result();
	}
	// Tambah Treatment
	public function tambah_treatment($data) {
		return $this->db->insert('tbl_rekam_treatment',$data);
	}
	// Hapus Treatment
	public function hapus_treatment($id) {
		$this->db->where('id',$id);
		$this->db->update('tbl_rekam_treatment',['deleted_at' => date("Y-m-d h:i:s")]);
	}	

	
	// Listing Rekam Produk
	public function listing_produk($id_rekam_medis) {
		$this->db->select("t1.*, t2.nama as nama_produk, t2.harga as harga_produk");
		$this->db->join('tbl_produk as t2','t1.id_produk=t2.id','LEFT');
		$query = $this->db->get_where('tbl_rekam_produk as t1', ['t1.id_rekam_medis' => $id_rekam_medis ,'t1.deleted_at' => NULL]);
		return $query->result();
	}
	// Tambah Produk
	public function tambah_produk($data) {
		return $this->db->insert('tbl_rekam_produk',$data);
	}
	// Ubah Produk
	public function ubah_produk($data) {
		$this->db->where('id',$data['id']);
		$this->db->update('tbl_rekam_produk',$data);
	}	
	// Hapus Produk
	public function hapus_produk($id) {
		$this->db->where('id',$id);
		$this->db->update('tbl_rekam_produk',['deleted_at' => date("Y-m-d h:i:s")]);
	}	
}

/* End of file Rekammedis_model.php */
/* Location: ./application/models/Rekammedis_model.php */