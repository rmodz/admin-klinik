<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_Model extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
		// Listing
	public function listing()
	{
		$query = $this->db->get_where('tbl_produk', ['deleted_at' => NULL]);
		return $query->result();
	}

	public function listing_report_produk($start, $end) {
		$this->db->join('tbl_produk', 'tbl_transaksi_detail.id_ref = tbl_produk.id', 'LEFT');
		$this->db->order_by('tbl_transaksi_detail.created_at', 'DESC');
		$this->db->where('tbl_transaksi_detail.created_at BETWEEN "'. date('Y-m-d', strtotime($start)) .' 00:00:00" AND "'. date('Y-m-d', strtotime($end)) .' 23:59:00"', null, false);
		$query = $this->db->get_where('tbl_transaksi_detail', ['tbl_transaksi_detail.deleted_at' => NULL, 'tbl_transaksi_detail.type' => 'T1']);
		return $query->result();
	}

		// Detail
	public function detail($id)
	{
		$query = $this->db->get_where('tbl_produk', ['deleted_at' => NULL, 'id' => $id]);
		return $query->row();
	}

	// Tambah
	public function tambah($data)
	{
		$this->db->insert('tbl_produk',$data);
	}

	// Edit
	public function edit($data)
	{
		$this->db->where('id',$data['id']);
		$this->db->update('tbl_produk',$data);
	}

	// Delete
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->update('tbl_produk',['deleted_at' => date("Y-m-d h:i:sa")]);
	}

}

/* End of file Produk_Model.php */
/* Location: ./application/models/Produk_Model.php */