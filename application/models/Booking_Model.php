<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_Model extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
		// Listing
	public function listing()
	{
		$this->db->select('tbl_booking.*,tbl_pelanggan.nama');
		$this->db->from('tbl_booking');
		$this->db->join('tbl_pelanggan','tbl_booking.id_pelanggan=tbl_pelanggan.id','LEFT');
		$this->db->where('tbl_booking.deleted_at', NULL);
		$this->db->order_by('tbl_booking.booking_date', "DESC");
		$query = $this->db->get();
		return $query->result();
	}

		// Detail
	public function detail($id)
	{
		$query = $this->db->get_where('tbl_booking', ['deleted_at' => NULL, 'id' => $id]);
		return $query->row();
	}

	// Tambah
	public function tambah($data)
	{
		$this->db->trans_start();
		$this->db->insert('tbl_booking',$data);

		// Tambahkan Otomatis ke Rekam Medis
		$this->db->insert('tbl_rekam_medis', ['id_pelanggan' => $data['id_pelanggan'], 'tanggal_hadir' => date('Y-m-d', strtotime($data['booking_date']))]);
		$this->db->trans_complete();
	}

	// Delete
	public function delete($id)
	{
		$this->db->trans_start();
		$data = $this->detail($id);
		// return $data;
		// return $data;
		$this->db->where('id',$data->id);
		$this->db->update('tbl_booking',['deleted_at' => date("Y-m-d h:i:s")]);

		// Update Rekam Medis Di Batalkan
		$this->db->update('tbl_rekam_medis', ['status' => 'A4'], ['id_pelanggan' => $data->id_pelanggan, 'tanggal_hadir' => date('Y-m-d', strtotime($data->booking_date))]);
		$this->db->trans_complete();
	}	

}

/* End of file Booking_Model.php */
/* Location: ./application/models/Booking_Model.php */