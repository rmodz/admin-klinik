<!DOCTYPE html>
<html style="height: 50%;">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Klinik Kecantikan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link href="<?php echo base_url()?>assets/Admin/images/Logo.png" rel="icon">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/Admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/Admin/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/Admin/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/Admin/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/Admin/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" style="background-color: #FFF;">
<div class="row" style="margin: 10% auto;">
  <div class="col col-sm-8">
    <img src="<?php echo base_url() ?>assets/Admin/images/logo.jpeg" style="width: 80%; margin-top: 10%; margin-left: 5%;">
  </div>
  <div class="col col-sm-4">
    <div class="login-box" style="margin: 15% auto; margin-right: 10%;">
  <div class="login-logo">
    <a href="<?php echo base_url() ?>assets/Admin/index2.html"><b>Halaman</b> Login</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Masukkan username dan password anda</p>

<!-- NOTIFIKASI -->
<?php
if ($this->session->flashdata('success')){
  echo '<div class="alert alert-success">';
  echo $this->session->flashdata('success');
  echo'</div>';
}

if ($this->session->flashdata('danger')){
  echo '<div class="alert alert-danger">';
  echo $this->session->flashdata('danger');
  echo'</div>';
}
// ERROR WARNING
  echo validation_errors('<div class="alert alert-warning">','</div>');
//FORM OPEN
  echo form_open(base_url('login'));
?>
      <div class="form-group has-feedback">
        <input type="text" name ="username" class="form-control" placeholder="username" style="border-radius: 5px; font-size: 16px;">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password" style="border-radius: 5px; font-size: 16px;">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <!-- <label>
              <input type="checkbox"> Remember Me
            </label> -->
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat btn-lg" style="border-radius: 5px; font-size: 16px;">Sign In</button>
        </div>
        <!-- /.col -->
      </div>

<!-- FORM CLOSE -->
      <?php
      echo form_close();
      ?>


    <!-- <a href="<?php echo base_url()?>">Kembali ke beranda</a><br> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->    
  </div>
</div>
<!-- <div class="login-logo" style="position: absolute;">
  <img src="<?php echo base_url() ?>assets/Admin/images/logo.jpeg" style="width: 60%; margin: 10% auto;">
</div> -->


<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>assets/Admin/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>assets/Admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url() ?>assets/Admin/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>