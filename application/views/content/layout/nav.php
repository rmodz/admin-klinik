<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url()?>assets/upload/1.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $user_aktif->username?></p>
          <p><?php echo $user_aktif->role?></p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">        
        <li class="treeview">
          <li class="<?=menu_active() == 'Dashboard' ? 'active':''?>"><a href="<?php echo base_url('Dashboard')?>">
           <i class="fa fa-home"></i><span> Home</span>
           </a></li>
        </li>

        <!-- Logika Hak Akses -->
        <li class="treeview <?=menu_active() == 'master' ? 'active menu-open':''?>">
          <a href="#">
            <i class="fa fa-database"></i> <span> Data Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" <?=menu_active() == 'master' ? 'style="display: block;"':''?>>
            <li class="<?=menu_active(2) == 'Pengguna' ? 'active':''?>"><a href="<?php echo base_url('master/Pengguna')?>"> Pengguna</a></li>
            <li class="<?=menu_active(2) == 'Pelanggan' ? 'active':''?>"><a href="<?php echo base_url('master/Pelanggan')?>"> Pasien</a></li>
            <li class="<?=menu_active(2) == 'Produk' ? 'active':''?>"><a href="<?php echo base_url('master/Produk')?>"> Produk</a></li>
            <li class="<?=menu_active(2) == 'Treatment' ? 'active':''?>"><a href="<?php echo base_url('master/Treatment')?>"> Treatment</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <li class="<?=menu_active() == 'Booking' ? 'active':''?>"><a href="<?php echo base_url('Booking')?>">
           <i class="fa fa-calendar-plus"></i><span> Booking</span>
           </a></li>
        </li>

        <li class="treeview">
          <li class="<?=menu_active() == 'RekamMedis' ? 'active':''?>"><a href="<?php echo base_url('RekamMedis')?>">
           <i class="fa fa-file-medical"></i><span> Rekam Medis</span>
           </a></li>
        </li>

        <li class="treeview">
          <li class="<?=menu_active() == 'Transaksi' ? 'active':''?>"><a href="<?php echo base_url('Transaksi')?>">
           <i class="fa fa-money-bill-alt"></i><span> Transaksi</span>
           </a></li>
        </li>

        <li class="treeview <?=menu_active() == 'report' ? 'active menu-open':''?>">
          <a href="#">
            <i class="fa fa-file"></i> <span> Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=menu_active(2) == 'Report_pelanggan' ? 'active':''?>"><a href="<?php echo base_url('report/Report_pelanggan')?>"> Laporan Pasien</a></li>
            <li class="<?=menu_active(2) == 'Report_treatment' ? 'active':''?>"><a href="<?php echo base_url('report/Report_treatment')?>"> Laporan Treatment</a></li>
            <li class="<?=menu_active(2) == 'Report_produk' ? 'active':''?>"><a href="<?php echo base_url('report/Report_produk')?>">Laporan Produk</a></li>
            <li class="<?=menu_active(2) == 'Report_transaksi' ? 'active':''?>"><a href="<?php echo base_url('report/Report_transaksi')?>">Laporan Transaksi</a></li>
          </ul>
        </li>
        <!-- End Validasi Hak Akses -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   <h1>
        <?php echo $title ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('Dashboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
            
          </a></li>
        <li class="active"> <?php echo $title ?></li>
      </ol>
    </section>

 <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
     <h3 class="box-title"><?php echo $title ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
