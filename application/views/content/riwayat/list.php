<?php
// Notifikasi
if($this->session->flashdata('success'))
{
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('success');
	echo '</div>';
}

if($this->session->flashdata('danger'))
{
	echo '<div class="alert alert-danger">';
	echo $this->session->flashdata('danger');
	echo '</div>';
}

if($this->session->flashdata('warning'))
{
	echo '<div class="alert alert-warning">';
	echo $this->session->flashdata('warning');
	echo '</div>';
}

// Error warning
echo validation_errors('<div class="alert alert-warning">','</div>');
?>
<div class="row">
	<div class="col-sm-12 text-right">
		<button type="button" title="Tambah" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah data riwayat</button>
	</div>
</div>
<br><hr>

<div class="box-body table-responsive no-padding">
<table id="example1" class="table table-hover table-bordered">
<thead>
<tr>
<th width="10%"><center>No</th></center>
<th><center>Keterangan</th></center>
<th width="30%"><center>Action</th></center>
</tr>
</thead>
<tbody>

<?php $i=1; foreach ($tbl_pelanggan as $riwayat_pelanggan) { ?>
<tr>
<td><center><?php echo $i?></td></center>
<td><?php echo $riwayat_pelanggan->keterangan ?></td>
<td><center>
    <button type="button" title="Hapus" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete<?=$riwayat_pelanggan->id?>"><i class="fa fa-trash"></i> Hapus</button>
</td></center>
</tr>
<?php $i++; } ?>

</tbody>
</table>

<div class="row">
	<div class="col-sm-12 text-right">
		<a href="<?php echo base_url('master/Pelanggan') ?>" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"> Kembali</i></a>
	</div>
</div>
<br>



<!-- Modal Tambah -->
<div class="modal modal-default fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $juduladd ?></h4>
      </div>

<div class="modal-body">
	<?php
	// attribut
	$attribut='class="form-horizontal"';

	// Form open
	echo form_open(base_url('master/Riwayat_pelanggan/tambah'),$attribut);
	?>
	<input type="hidden" name="id_pelanggan" value="<?php echo $id_pelanggan; ?>" class="form-control">
	<div class="form-group">
	    <label class="col-sm-3 control-label">Keterangan</label>
	  <div class="col-sm-9">
	    <input type="text" name="keterangan" class="form-control" placeholder="Masukan Keterangan Riwayat Pasien" value="<?php echo set_value('keterangan')?>" required>
	  </div>
	</div>

	<div class="form-group">
	  	<label class="col-sm-3 control-label"></label>
			<div class="col-sm-9">
				<input type="submit" class="btn btn-success" name="submit" value="simpan data">
			</div>
	</div>

	<?php 
	// Form close
	echo form_close();
	?>
</div>

	  <div class="modal-footer">
	    <button type="button" class="btn btn-success pull-right" data-dismiss="modal">
	      <i class="fa fa-times"></i> Close</button>
	  </div>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Modal Edit -->

<!-- Modal Delete -->
<?php $i=1; foreach ($tbl_pelanggan as $riwayat_pelanggan) { ?>
<div class="modal modal-danger fade" id="delete<?php echo $riwayat_pelanggan->id?>">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $juduldelete ?></h4>
          </div>
          <div class="modal-body">
            <p>Yakin ingin menghapus data ini ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <a href="<?php echo base_url('master/Riwayat_pelanggan/delete/'.$riwayat_pelanggan->id)?>"class="btn btn-outline pull-right">
              <i class="fa fa-trash-o"></i> Ya, Hapus Data ini
            </a>
            <button type="button" class="btn btn-outline pull-right" data-dismiss="modal"><i class="fa fa-backward"></i> Tidak, Batalkan</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php $i++; } ?>
<!-- End Modal Delete -->