<div class="row">
	<div class="col col-md-6 col-sm-12">
		<div class="box box-solid box-info">
			<div class="box-header with-border">
			  <h3 class="box-title"><b>DATA PASIEN</b></h3>
			</div>

			<div class="box-body">
				<div class="row">
			    	<div class="col col-sm-12">
					    <div class="form-group">
					      <label for="inputAge">Nama Pasien</label>
				        <input type="text" class="form-control" id="inputAge" placeholder="Usia" readonly value="<?php echo $tbl_rekam_medis->nama ?>">
					    </div>
			    	</div>
			    </div>

		    <div class="row">
		    	<div class="col col-sm-6">
				    <div class="form-group">
				      <label for="inputAge">Usia</label>
			        <input type="text" class="form-control" id="inputAge" placeholder="Usia" readonly value="<?php echo convertBirtdateToAge($tbl_rekam_medis->tanggal_lahir) ?>">
				    </div>
		    	</div>

		    	<div class="col col-sm-6">
				    <div class="form-group">
				      <label for="inputPhone">Telepon</label>
			        <input type="text" class="form-control" id="inputPhone" placeholder="Telepon" readonly value="<?php echo $tbl_rekam_medis->no_hp ?>">
				    </div>
		    	</div>
		    </div>

		    <div class="form-group">
		      <label for="inputJob">Pekerjaan</label>
	        <input type="text" class="form-control" id="inputJob" placeholder="Pekerjaan" readonly value="<?php echo $tbl_rekam_medis->pekerjaan ?>">
		    </div>

		    <div class="form-group">
		      <label for="inputAddress">Alamat</label>
	      	<textarea class="form-control" id="inputAddress" placeholder="Alamat" rows="5" readonly><?php echo $tbl_rekam_medis->alamat ?></textarea>
		    </div>

			</div>
		</div>

		<div class="box box-solid box-info">
			<div class="box-header with-border">
	  		<h3 class="box-title"><b>Tatalaksana</b></h3>
			</div>
			<div class="box-body table-responsive">
				<table class="table table-hover table-bordered dtable">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Nama Treatment</th>
							<th>Harga</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($tbl_rekam_treatment as $key => $value): ?>
							<tr>
								<th class="text-center"><?=$key+1?></th>
								<th><?=$value->nama_treatment?></th>
								<th><?=number_format($value->harga_treatment,0,',','.')?></th>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col col-md-6 col-sm-12">
		<div class="box box-solid box-info">
			<div class="box-header with-border">
			  <h3 class="box-title"><b>ANALISA KULIT</b></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col col-md-12">
						<div class="form-group">
					      	<label for="inputKelUtama">Keluhan Utama</label>
				      		<textarea class="form-control" id="inputKelUtama" placeholder="Keluhan Utama" rows="2" name="keluhan_utama" readonly><?=$tbl_rekam_medis->keluhan_utama?></textarea>
				    	</div>

				    	<div class="form-group">
					      	<label for="inputKelTambahan">Keluhan Tambahan</label>
				      		<textarea class="form-control" id="inputKelTambahan" placeholder="Keluhan Tambahan" rows="2" name="keluhan_tambahan" readonly><?=$tbl_rekam_medis->keluhan_tambahan?></textarea>
				    	</div>
					</div>
				</div>

				<div class="row">
					<div class="col col-md-6">
						<div class="form-group">
					      	<label>Jenis Kulit</label>
					      	<div class="radio">
					            <label><input type="radio" disabled name="jenis_kulit" value="kering" <?php if($tbl_rekam_medis->jenis_kulit == "kering") echo "checked"; ?>> Kering</label>
					            <label><input type="radio" disabled name="jenis_kulit" value="normal" <?php if($tbl_rekam_medis->jenis_kulit == "normal") echo "checked"; ?>> Normal</label>
					            <label><input type="radio" disabled name="jenis_kulit" value="berminyak" <?php if($tbl_rekam_medis->jenis_kulit == "berminyak") echo "checked"; ?>> Berminyak</label>
					            <label><input type="radio" disabled name="jenis_kulit" value="kombinasi" <?php if($tbl_rekam_medis->jenis_kulit == "kombinasi") echo "checked"; ?>> Kombinasi</label>
					            <label><input type="radio" disabled name="jenis_kulit" value="sensitive" <?php if($tbl_rekam_medis->jenis_kulit == "sensitive") echo "checked"; ?>> Sensitive</label>
				          	</div>
					    </div>						
					</div>

					<div class="col col-md-6">
						<div class="form-group">
						    <label for="inputEmail3" >Kelembaban</label>
					      	<div class="radio">
					            <label><input type="radio"  disabled name="kelembapan" value="kurang" <?php if($tbl_rekam_medis->kelembapan == "kurang") echo "checked";?>> Kurang</label>
					            <label> <input type="radio" disabled name="kelembapan" value="cukup" <?php if($tbl_rekam_medis->kelembapan == "cukup") echo "checked";?>> Cukup</label>
					            <label> <input type="radio" disabled name="kelembapan" value="baik" <?php if($tbl_rekam_medis->kelembapan == "baik") echo "checked";?>> Baik</label>
				          	</div>
					    </div>
					</div>
				</div>

		    <div class="form-group">
		      	<label for="tipeKulit">Tipe Kulit</label>
	        	<input readonly type="text" class="form-control" id="tipeKulit" placeholder="Tipe Kulit" name="type_kulit" value="<?=$tbl_rekam_medis->type_kulit?>">
		    </div>

		    <div class="form-group">
		      	<label for="inputGlogaus">Glogaus</label>
	        	<input readonly type="text" class="form-control" id="inputGlogaus" placeholder="Glogaus" name="glogaus" value="<?=$tbl_rekam_medis->glogaus?>">
		    </div>

		    <div class="form-group">
		      	<label for="inputKekenyalan">Kekenyalan</label>
	        	<input readonly type="text" class="form-control" id="inputKekenyalan" placeholder="Kekenyalan" name="kekenyalan" value="<?=$tbl_rekam_medis->kekenyalan?>">
		    </div>

		    <div class="form-group">
		      	<label for="inputTonus">Tonus / Turgor</label>
	        	<input readonly type="text" class="form-control" id="inputTonus" placeholder="Tonus / Turgor" name="tonus_turgor" value="<?=$tbl_rekam_medis->tonus_turgor?>">
		    </div>

		    <div class="row">
		    	<div class="col col-sm-6">
				    <div class="form-group">
					    <label>Kerutan</label>
				      	<div class="checkbox">
			            	<label><input type="checkbox" disabled  name="kerutan[]" value="dahi" <?php if(populateJsonCheckbox("dahi", $tbl_rekam_medis->kerutan)) echo "checked"; ?>> Dahi</label>
				        </div>
			          	<div class="checkbox">
			            	<label><input type="checkbox" disabled  name="kerutan[]" value="glabelar" <?php if(populateJsonCheckbox("glabelar", $tbl_rekam_medis->kerutan)) echo "checked"; ?>> Glabelar</label>
			          	</div>
			          	<div class="checkbox">
			            	<label><input type="checkbox" disabled  name="kerutan[]" value="sekitar_mata" <?php if(populateJsonCheckbox("sekitar_mata", $tbl_rekam_medis->kerutan)) echo "checked"; ?>> Sekitar Mata</label>
			          	</div>
			          	<div class="checkbox">  
			            	<label><input type="checkbox" disabled  name="kerutan[]" value="sekitar_mulut" <?php if(populateJsonCheckbox("sekitar_mulut", $tbl_rekam_medis->kerutan)) echo "checked"; ?>> Sekitar Mulut</label>
			          	</div>
				    </div>
		    	</div>

		    	<div class="col col-sm-6">
				    <div class="form-group">
				      <label>Kelainan Kulit</label>
				      <div class="row">
				      	<div class="col col-sm-6 col-xs-12">
					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled  name="kelainan_kulit[]" value="pori_pori_besar" <?php if(populateJsonCheckbox("pori_pori_besar", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Pori - pori Besar</label>
				          	</div>
					      	
					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled  name="kelainan_kulit[]" value="hiperpigmentasi" <?php if(populateJsonCheckbox("hiperpigmentasi", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Hiperpigmentasi</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled name="kelainan_kulit[]" value="hipopigmentasi" <?php if(populateJsonCheckbox("hipopigmentasi", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Hipopigmentasi</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled name="kelainan_kulit[]" value="komedo" <?php if(populateJsonCheckbox("komedo", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Komedo</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled name="kelainan_kulit[]" value="milia" <?php if(populateJsonCheckbox("milia", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Milia</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled name="kelainan_kulit[]" value="akne" <?php if(populateJsonCheckbox("akne", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Akne</label>
				          	</div>
				      	</div>

				      	<div class="col col-sm-6 col-xs-12">
					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled name="kelainan_kulit[]" value="kutil" <?php if(populateJsonCheckbox("kutil", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Kutil</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled name="kelainan_kulit[]" value="scar" <?php if(populateJsonCheckbox("scar", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Scar</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled name="kelainan_kulit[]" value="keloid" <?php if(populateJsonCheckbox("keloid", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Keloid</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled name="kelainan_kulit[]" value="kantung_mata" <?php if(populateJsonCheckbox("kantung_mata", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Kantung Mata</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" disabled name="kelainan_kulit[]" value="keratosis" <?php if(populateJsonCheckbox("keratosis", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Keratosis</label>
				          	</div>
				      	</div>
				      </div>
			    	</div>
			    </div>
			  </div>

			<div class="form-group">
		      	<label for="inputDiagnosis">Diagnosis</label>
	      		<textarea class="form-control" id="inputDiagnosis" placeholder="Diagnosis" rows="4" name="diagnosis" readonly><?=$tbl_rekam_medis->diagnosis?></textarea>
	    	</div>

			</div>
		</div>
	</div>
</div>

<div class="box box-solid box-info">
	<div class="box-header with-border">
		<h3 class="box-title"><b>Home Care</b></h3>
	</div>

	<div class="box-body table-responsive">
		<div class="row">
			<div class="col col-sm-6">
				<label>Pagi</label>
				<table class="table table-hover table-bordered dtable">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Nama Produk</th>
							<th>Harga</th>
							<th>Jumlah</th>
						</tr>
					</thead>
					<tbody>
						<?php $index = 1; ?>
						<?php foreach ($tbl_rekam_produk as $value): ?>
							<?php if ($value->waktu == "P"): ?>
								<tr>
									<th class="text-center"><?=$index?></th>
									<th><?=$value->nama_produk?></th>
									<th><?=number_format($value->harga_produk,0,',','.')?></th>
									<th><?=$value->jumlah?></th>
								</tr>
							<?php $index++; ?>
							<?php endif ?>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<div class="col col-sm-6">
				<label>Malam</label>
				<table class="table table-hover table-bordered dtable">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Nama Produk</th>
							<th>Harga</th>
							<th>Jumlah</th>
						</tr>
					</thead>
					<tbody>
						<?php $index1 = 1; ?>
						<?php foreach ($tbl_rekam_produk as $value): ?>
							<?php if ($value->waktu == "M"): ?>
								<tr>
									<th class="text-center"><?=$index1?></th>
									<th><?=$value->nama_produk?></th>
									<th><?=number_format($value->harga_produk,0,',','.')?></th>
									<th><?=$value->jumlah?></th>
								</tr>
							<?php $index1++; ?>
							<?php endif ?>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 text-right">
    <a class="btn btn-default btn-lg" href="<?=site_url('RekamMedis')?>" style="border-radius: 5px; margin-right: 10px;"><i class="fa fa-arrow-left"></i> Kembali</a>
	</div>
</div>