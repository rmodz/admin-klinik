<?php 
// Notifikasi
if($this->session->flashdata('success'))
{
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('success');
	echo '</div>';
}

if($this->session->flashdata('danger'))
{
	echo '<div class="alert alert-danger">';
	echo $this->session->flashdata('danger');
	echo '</div>';
}

if($this->session->flashdata('warning'))
{
	echo '<div class="alert alert-warning">';
	echo $this->session->flashdata('warning');
	echo '</div>';
}
// Error warning
echo validation_errors('<div class="alert alert-warning">','</div>');

// Atribut
// $attribut = 'class="alert alert-info"';
$attribut = '';
// Form open
echo form_open( base_url('RekamMedis/edit/'.$tbl_rekam_medis->id),$attribut);

// Csrf protection = Perlindungan terhadap form dari serangan hacker

?>

<div class="row">
	<div class="col col-md-6 col-sm-12">
		<div class="box box-solid box-info">
			<div class="box-header with-border">
			  <h3 class="box-title"><b>DATA PASIEN</b></h3>
			</div>

			<div class="box-body">
				<div class="row">
			    	<div class="col col-sm-12">
					    <div class="form-group">
					      <label for="inputAge">Nama Pasien</label>
				        <input type="text" class="form-control" id="inputAge" placeholder="Usia" readonly value="<?php echo $tbl_rekam_medis->nama ?>">
					    </div>
			    	</div>
			    </div>

		    <div class="row">
		    	<div class="col col-sm-6">
				    <div class="form-group">
				      <label for="inputAge">Usia</label>
			        <input type="text" class="form-control" id="inputAge" placeholder="Usia" readonly value="<?php echo convertBirtdateToAge($tbl_rekam_medis->tanggal_lahir) ?>">
				    </div>
		    	</div>

		    	<div class="col col-sm-6">
				    <div class="form-group">
				      <label for="inputPhone">Telepon</label>
			        <input type="text" class="form-control" id="inputPhone" placeholder="Telepon" readonly value="<?php echo $tbl_rekam_medis->no_hp ?>">
				    </div>
		    	</div>
		    </div>

		    <div class="form-group">
		      <label for="inputJob">Pekerjaan</label>
	        <input type="text" class="form-control" id="inputJob" placeholder="Pekerjaan" readonly value="<?php echo $tbl_rekam_medis->pekerjaan ?>">
		    </div>

		    <div class="form-group">
		      <label for="inputAddress">Alamat</label>
	      	<textarea class="form-control" id="inputAddress" placeholder="Alamat" rows="5" readonly><?php echo $tbl_rekam_medis->alamat ?></textarea>
		    </div>

			</div>
		</div>

		<div class="box box-solid box-info">
			<div class="box-header with-border">
				<div class="row">
					<div class="col col-sm-6">
			  		<h3 class="box-title"><b>Tatalaksana</b></h3>
					</div>

					<div class="col col-sm-6 text-right">
			  		<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#tambah-tatalaksana"><i class="fa fa-plus"></i> Tambah Data</button>
					</div>
				</div>
			</div>
			<div class="box-body table-responsive">
				<table class="table table-hover table-bordered dtable">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Nama Treatment</th>
							<th>Harga</th>
							<th style="width: 15%;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($tbl_rekam_treatment as $key => $value): ?>
							<tr>
								<th class="text-center"><?=$key+1?></th>
								<th><?=$value->nama_treatment?></th>
								<th><?=number_format($value->harga_treatment,0,',','.')?></th>
								<th class="text-center">
									<a class="btn btn-danger btn-xs" href="<?=site_url('RekamMedis/delete_treatment/'.$value->id.'/'.$tbl_rekam_medis->id)?>"><i class="fa fa-trash"></i></a>
								</th>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col col-md-6 col-sm-12">
		<div class="box box-solid box-info">
			<div class="box-header with-border">
			  <h3 class="box-title"><b>ANALISA KULIT</b></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col col-md-12">
						<div class="form-group">
					      	<label for="inputKelUtama">Keluhan Utama</label>
				      		<textarea class="form-control" id="inputKelUtama" placeholder="Keluhan Utama" rows="2" name="keluhan_utama" required><?=$tbl_rekam_medis->keluhan_utama?></textarea>
				    	</div>

				    	<div class="form-group">
					      	<label for="inputKelTambahan">Keluhan Tambahan</label>
				      		<textarea class="form-control" id="inputKelTambahan" placeholder="Keluhan Tambahan" rows="2" name="keluhan_tambahan"><?=$tbl_rekam_medis->keluhan_tambahan?></textarea>
				    	</div>
					</div>
				</div>

				<div class="row">
					<div class="col col-md-6">
						<div class="form-group">
					      	<label>Jenis Kulit</label>
					      	<div class="radio">
					            <label><input type="radio" name="jenis_kulit" value="kering" <?php if($tbl_rekam_medis->jenis_kulit == "kering") echo "checked"; ?>> Kering</label>
					            <label><input type="radio" name="jenis_kulit" value="normal" <?php if($tbl_rekam_medis->jenis_kulit == "normal") echo "checked"; ?>> Normal</label>
					            <label><input type="radio" name="jenis_kulit" value="berminyak" <?php if($tbl_rekam_medis->jenis_kulit == "berminyak") echo "checked"; ?>> Berminyak</label>
					            <label><input type="radio" name="jenis_kulit" value="kombinasi" <?php if($tbl_rekam_medis->jenis_kulit == "kombinasi") echo "checked"; ?>> Kombinasi</label>
					            <label><input type="radio" name="jenis_kulit" value="sensitive" <?php if($tbl_rekam_medis->jenis_kulit == "sensitive") echo "checked"; ?>> Sensitive</label>
				          	</div>
					    </div>						
					</div>

					<div class="col col-md-6">
						<div class="form-group">
						    <label for="inputEmail3" >Kelembaban</label>
					      	<div class="radio">
					            <label><input type="radio"  name="kelembapan" value="kurang" <?php if($tbl_rekam_medis->kelembapan == "kurang") echo "checked";?>> Kurang</label>
					            <label> <input type="radio" name="kelembapan" value="cukup" <?php if($tbl_rekam_medis->kelembapan == "cukup") echo "checked";?>> Cukup</label>
					            <label> <input type="radio" name="kelembapan" value="baik" <?php if($tbl_rekam_medis->kelembapan == "baik") echo "checked";?>> Baik</label>
				          	</div>
					    </div>
					</div>
				</div>

		    <div class="form-group">
		      	<label for="tipeKulit">Tipe Kulit</label>
	        	<input type="text" class="form-control" id="tipeKulit" placeholder="Tipe Kulit" name="type_kulit" value="<?=$tbl_rekam_medis->type_kulit?>">
		    </div>

		    <div class="form-group">
		      	<label for="inputGlogaus">Glogaus</label>
	        	<input type="text" class="form-control" id="inputGlogaus" placeholder="Glogaus" name="glogaus" value="<?=$tbl_rekam_medis->glogaus?>">
		    </div>

		    <div class="form-group">
		      	<label for="inputKekenyalan">Kekenyalan</label>
	        	<input type="text" class="form-control" id="inputKekenyalan" placeholder="Kekenyalan" name="kekenyalan" value="<?=$tbl_rekam_medis->kekenyalan?>">
		    </div>

		    <div class="form-group">
		      	<label for="inputTonus">Tonus / Turgor</label>
	        	<input type="text" class="form-control" id="inputTonus" placeholder="Tonus / Turgor" name="tonus_turgor" value="<?=$tbl_rekam_medis->tonus_turgor?>">
		    </div>

		    <div class="row">
		    	<div class="col col-sm-6">
				    <div class="form-group">
					    <label>Kerutan</label>
				      	<div class="checkbox">
			            	<label><input type="checkbox"  name="kerutan[]" value="dahi" <?php if(populateJsonCheckbox("dahi", $tbl_rekam_medis->kerutan)) echo "checked"; ?>> Dahi</label>
				        </div>
			          	<div class="checkbox">
			            	<label><input type="checkbox"  name="kerutan[]" value="glabelar" <?php if(populateJsonCheckbox("glabelar", $tbl_rekam_medis->kerutan)) echo "checked"; ?>> Glabelar</label>
			          	</div>
			          	<div class="checkbox">
			            	<label><input type="checkbox"  name="kerutan[]" value="sekitar_mata" <?php if(populateJsonCheckbox("sekitar_mata", $tbl_rekam_medis->kerutan)) echo "checked"; ?>> Sekitar Mata</label>
			          	</div>
			          	<div class="checkbox">  
			            	<label><input type="checkbox"  name="kerutan[]" value="sekitar_mulut" <?php if(populateJsonCheckbox("sekitar_mulut", $tbl_rekam_medis->kerutan)) echo "checked"; ?>> Sekitar Mulut</label>
			          	</div>
				    </div>
		    	</div>

		    	<div class="col col-sm-6">
				    <div class="form-group">
				      <label>Kelainan Kulit</label>
				      <div class="row">
				      	<div class="col col-sm-6 col-xs-12">
					      	<div class="checkbox">
				            	<label><input type="checkbox"  name="kelainan_kulit[]" value="pori_pori_besar" <?php if(populateJsonCheckbox("pori_pori_besar", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Pori - pori Besar</label>
				          	</div>
					      	
					      	<div class="checkbox">
				            	<label><input type="checkbox"  name="kelainan_kulit[]" value="hiperpigmentasi" <?php if(populateJsonCheckbox("hiperpigmentasi", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Hiperpigmentasi</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" name="kelainan_kulit[]" value="hipopigmentasi" <?php if(populateJsonCheckbox("hipopigmentasi", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Hipopigmentasi</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" name="kelainan_kulit[]" value="komedo" <?php if(populateJsonCheckbox("komedo", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Komedo</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" name="kelainan_kulit[]" value="milia" <?php if(populateJsonCheckbox("milia", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Milia</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" name="kelainan_kulit[]" value="acne" <?php if(populateJsonCheckbox("acne", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Acne</label>
				          	</div>
				      	</div>

				      	<div class="col col-sm-6 col-xs-12">
					      	<div class="checkbox">
				            	<label><input type="checkbox" name="kelainan_kulit[]" value="kutil" <?php if(populateJsonCheckbox("kutil", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Kutil</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" name="kelainan_kulit[]" value="scar" <?php if(populateJsonCheckbox("scar", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Scar</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" name="kelainan_kulit[]" value="keloid" <?php if(populateJsonCheckbox("keloid", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Keloid</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" name="kelainan_kulit[]" value="kantung_mata" <?php if(populateJsonCheckbox("kantung_mata", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Kantung Mata</label>
				          	</div>

					      	<div class="checkbox">
				            	<label><input type="checkbox" name="kelainan_kulit[]" value="keratosis" <?php if(populateJsonCheckbox("keratosis", $tbl_rekam_medis->kelainan_kulit)) echo "checked"; ?>> Keratosis</label>
				          	</div>
				      	</div>
				      </div>
			    	</div>
			    </div>
			  </div>

			<div class="form-group">
		      	<label for="inputDiagnosis">Diagnosis</label>
	      		<textarea class="form-control" id="inputDiagnosis" placeholder="Diagnosis" rows="4" name="diagnosis"><?=$tbl_rekam_medis->diagnosis?></textarea>
	    	</div>

			</div>
		</div>
	</div>
</div>

<div class="box box-solid box-info">
	<div class="box-header with-border">
		<div class="row">
			<div class="col col-sm-6">
	  		<h3 class="box-title"><b>Home Care</b></h3>
			</div>

			<div class="col col-sm-6 text-right">
	  		<button class="btn btn-success btn-xs" type="button" data-toggle="modal" data-target="#tambah-home-care"><i class="fa fa-plus"></i> Tambah Data</button>
			</div>
		</div>
	</div>

	<div class="box-body table-responsive">
		<div class="row">
			<div class="col col-sm-6">
				<label>Pagi</label>
				<table class="table table-hover table-bordered dtable">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Nama Produk</th>
							<th>Jumlah</th>
							<th>Total Harga</th>
							<th style="width: 15%;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $index = 1; ?>
						<?php foreach ($tbl_rekam_produk as $value): ?>
							<?php if ($value->waktu == "P"): ?>
								<tr>
									<th class="text-center"><?=$index?></th>
									<th><?=$value->nama_produk?> (@<?=number_format($value->harga_produk,0,',','.')?>)</th>
									<th><?=$value->jumlah?></th>
									<th><?=number_format($value->harga_produk * $value->jumlah,0,',','.')?></th>
									<th class="text-center">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#edit-home-care-<?=$value->id?>"><i class="fa fa-edit"></i></button>
										<a class="btn btn-danger btn-xs" href="<?=site_url('RekamMedis/delete_produk/'.$value->id.'/'.$tbl_rekam_medis->id)?>"><i class="fa fa-trash"></i></a>
									</th>
								</tr>
							<?php $index++; ?>
							<?php endif ?>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<div class="col col-sm-6">
				<label>Malam</label>
				<table class="table table-hover table-bordered dtable">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Nama Produk</th>
							<th>Jumlah</th>
							<th>Total Harga</th>
							<th style="width: 15%;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $index1 = 1; ?>
						<?php foreach ($tbl_rekam_produk as $value): ?>
							<?php if ($value->waktu == "M"): ?>
								<tr>
									<th class="text-center"><?=$index1?></th>
									<th><?=$value->nama_produk?> (@<?=number_format($value->harga_produk,0,',','.')?>)</th>
									<th><?=$value->jumlah?></th>
									<th><?=number_format($value->harga_produk * $value->jumlah,0,',','.')?></th>
									<th class="text-center">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#edit-home-care-<?=$value->id?>"><i class="fa fa-edit"></i></button>
										<a class="btn btn-danger btn-xs" href="<?=site_url('RekamMedis/delete_produk/'.$value->id.'/'.$tbl_rekam_medis->id)?>"><i class="fa fa-trash"></i></a>
									</th>
								</tr>
							<?php $index1++; ?>
							<?php endif ?>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 text-right">
    <a class="btn btn-default btn-lg" href="<?=site_url('RekamMedis')?>" style="border-radius: 5px; margin-right: 10px;"><i class="fa fa-arrow-left"></i> Kembali</a>
    	<input type="submit" name="submit" value="simpan" title="Simpan" class="btn btn-info btn-lg" style="margin-right: 10px;">

    	<input type="submit" name="submit" value="selesai" title="Simpan & Selesai" class="btn btn-info btn-lg">
	</div>
</div>
<?=form_close()?>

<div class="modal modal-default fade" id="tambah-tatalaksana">
  <div class="modal-dialog">
    <div class="modal-content">
			<?=form_open(base_url('RekamMedis/tambah_treatment'), 'class="form-horizontal"')?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Tatalaksana</h4>
      </div>

			<div class="modal-body">
				<input type="hidden" name="id_rekam_medis" value="<?=$tbl_rekam_medis->id?>">
				<div class="form-group">
	    			<label class="col-sm-3 control-label">Treatment</label>
		  			<div class="col-sm-9">
			            <select name="id_treatment" class="form-control select2" style="width: 100%; height: 35px;" required>
			              <option value="">- Pilih -</option>
			              <?php foreach ($tbl_treatment as $value) { ?>
			              <option value="<?php echo $value->id ?>">
			              <?php echo $value->nama ?>
			              </option>
			              <?php } ?>
			            </select>
					</div>
				</div>

				<div class="form-group">
				  	<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9" style="margin-top: 10px;">
							<input type="submit" class="btn btn-success" name="submit" value="simpan data">
						</div>
				</div>

				<?=form_close()?>
			</div>

	  	<div class="modal-footer">
	    	<button type="button" class="btn btn-success pull-right" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
	  	</div>

    </div>
  </div>
</div>

<div class="modal modal-default fade" id="tambah-home-care">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Home Care</h4>
      </div>

			<div class="modal-body">
				<?=form_open(base_url('RekamMedis/tambah_produk'), 'class="form-horizontal"')?>
				<input type="hidden" name="id_rekam_medis" value="<?=$tbl_rekam_medis->id?>">
				<div class="form-group">
		      <label class="col-sm-3 control-label">Pilih Waktu</label>
	      	<div class="radio col-sm-9">
            <label style="margin-right: 10px;"><input type="radio" name="waktu" value="P" checked> Pagi</label>
            <label> <input type="radio" name="waktu" value="M"> Malam</label>
          </div>
		    </div>

				<div class="form-group">
	    		<label class="col-sm-3 control-label">Produk</label>
	  			<div class="col-sm-9">
            <select name="id_produk" class="form-control select2" style="width: 100%; height: 35px;" required>
            	  <option value="">- Pilih -</option>
	              <?php foreach ($tbl_produk as $value) { ?>
	              <option value="<?php echo $value->id ?>">
	              <?php echo $value->nama ?>
	              </option>
	              <?php } ?>
            </select>
				  </div>
				</div>

				<div class="form-group">
	    		<label class="col-sm-3 control-label">Jumlah</label>
	  			<div class="col-sm-9">
            <input type="number" class="form-control" name="jumlah" value="1" required onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
				  </div>
				</div>

				<div class="form-group">
				  	<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<input type="submit" class="btn btn-success" name="submit" value="simpan data">
						</div>
				</div>

				<?=form_close()?>
			</div>

	  	<div class="modal-footer">
	    	<button type="button" class="btn btn-success pull-right" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
	  	</div>

    </div>
  </div>
</div>

<?php foreach ($tbl_rekam_produk as $key => $value): ?>
<div class="modal modal-default fade" id="edit-home-care-<?=$value->id?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Home Care</h4>
      </div>

			<div class="modal-body">
				<?=form_open(base_url('RekamMedis/ubah_produk'), 'class="form-horizontal"')?>
				<input type="hidden" name="id" value="<?=$value->id?>">
				<input type="hidden" name="id_rekam_medis" value="<?=$tbl_rekam_medis->id?>">
				<div class="form-group">
		      <label class="col-sm-3 control-label">Pilih Waktu</label>
	      	<div class="radio col-sm-9">
            <label style="margin-right: 10px;"><input type="radio" name="waktu" value="P" <?=$value->waktu == "P" ?"checked":""?>> Pagi</label>
            <label> <input type="radio" name="waktu" value="M" <?=$value->waktu == "M" ?"checked":""?>> Malam</label>
          </div>
		    </div>

				<div class="form-group">
	    		<label class="col-sm-3 control-label">Produk</label>
	  			<div class="col-sm-9">
            <select name="id_produk" class="form-control select2" style="width: 100%; height: 35px;" required>
            	  <option value="">- Pilih -</option>
	              <?php foreach ($tbl_produk as $val) { ?>
	              <option value="<?php echo $val->id ?>" <?=$value->id_produk == $val->id ? "selected":""?>>
	              <?php echo $val->nama ?>
	              </option>
	              <?php } ?>
            </select>
				  </div>
				</div>

				<div class="form-group">
	    		<label class="col-sm-3 control-label">Jumlah</label>
	  			<div class="col-sm-9">
            <input type="number" class="form-control" name="jumlah" value="1" required value="<?=$value->jumlah?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
				  </div>
				</div>

				<div class="form-group">
				  	<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<input type="submit" class="btn btn-success" name="submit" value="simpan data">
						</div>
				</div>

				<?=form_close()?>
			</div>

	  	<div class="modal-footer">
	    	<button type="button" class="btn btn-success pull-right" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
	  	</div>

    </div>
  </div>
</div>
<?php endforeach ?>