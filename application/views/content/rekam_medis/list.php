<?php
// Notifikasi
if($this->session->flashdata('success'))
{
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('success');
	echo '</div>';
}

if($this->session->flashdata('danger'))
{
	echo '<div class="alert alert-danger">';
	echo $this->session->flashdata('danger');
	echo '</div>';
}

if($this->session->flashdata('warning'))
{
	echo '<div class="alert alert-warning">';
	echo $this->session->flashdata('warning');
	echo '</div>';
}

// Error warning
echo validation_errors('<div class="alert alert-warning">','</div>');

// Include tambah
?>

<div class="row">
	<div class="col-sm-4">
		<label>Sortir Tanggal </label>
		<form method="POST" action="<?=site_url('RekamMedis')?>">
			<input name="filter" type="date" class="form-control" onchange="this.form.submit()" value="<?=$date?>">
		</form>
	</div>

	<div class="col-sm-8 text-right" style="padding-top: 25px;">
		<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah Data</button>
	</div>
</div>

<hr style="margin-bottom: 30px;">

<div class="table-responsive no-padding">
	<table id="example1" class="table table-hover table-bordered">
		<thead>
			<tr>
				<th class="text-center" width="5%">No</th>
				<th class="text-center">Nama Pelanggan</th>
				<th class="text-center">Tanggal Hadir</th>
				<th class="text-center">Status</th>
				<th class="text-center" style="width: 15%;">Action</th>
			</tr>
		</thead>
		
		<tbody>
			<?php $i=1; foreach ($tbl_rekam_medis as $rekam_medis) { ?>
			<tr>
				<td class="text-center"><?php echo $i?></td>
				<td><?php echo $rekam_medis->nama ?></td>
				<td><?php echo date("d F Y", strtotime($rekam_medis->tanggal_hadir)) ?></td>
				<td class="text-center"><span class="label label-primary"><?php echo rekam_medis_status($rekam_medis->status); ?></span></td>
				<td class="text-center">
					<?php if ($rekam_medis->status != "A1"): ?>
					<a href="<?=site_url('RekamMedis/view/'.$rekam_medis->id)?>" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> Lihat</a>
					<?php else: ?>
					<a href="<?=site_url('RekamMedis/edit/'.$rekam_medis->id)?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Edit</a>
					<?php endif ?>
				  <button type="button" title="Hapus" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete<?=$rekam_medis->id?>"><i class="fa fa-trash"></i> Hapus</button>
				</td>
			</tr>
			<div class="modal modal-danger fade" id="delete<?php echo $rekam_medis->id?>">
	      <div class="modal-dialog">
	        <div class="modal-content">
	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	              <span aria-hidden="true">&times;</span></button>
	            <h4 class="modal-title"><?php echo $juduldelete ?></h4>
	          </div>
	          <div class="modal-body">
	            <p>Yakin ingin menghapus data ini ?</p>
	          </div>
	          <div class="modal-footer">
	            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
	            <a href="<?php echo base_url('RekamMedis/delete/'.$rekam_medis->id)?>"class="btn btn-outline pull-right">
	              <i class="fa fa-trash-o"></i> Ya, Hapus Data ini
	            </a>
	            <button type="button" class="btn btn-outline pull-right" data-dismiss="modal"><i class="fa fa-backward"></i> Tidak, Batalkan</button>
	          </div>
	        </div>
	        <!-- /.modal-content -->
	      </div>
	      <!-- /.modal-dialog -->
	    </div>
	    <!-- /.modal -->
			<?php $i++; } ?>
		</tbody>
	</table>
</div>

<!-- Modal Tambah -->
<div class="modal modal-default fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $juduladd ?></h4>
      </div>

			<div class="modal-body">
				<?=form_open(base_url('RekamMedis/tambah'), 'class="form-horizontal"')?>
				<div class="row">
					<div class="col-sm-12 text-right" style="margin-bottom: 10px;">
						<a href="<?php echo base_url('master/Pelanggan') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Pasien Baru</a>
					</div>
				</div>
				<div class="form-group">
	    		<label class="col-sm-3 control-label">Nama Pelanggan</label>
	  			<div class="col-sm-9">
            <select name="id_pelanggan" class="form-control select2" style="width: 100%; height: 35px;" required>
              <option value="">- Pilih -</option>
              <?php foreach ($tbl_pelanggan as $value) { ?>
              <option value="<?php echo $value->id ?>">
              <?php echo $value->nama ?> - <?php echo $value->no_hp ?>
              </option>
              <?php } ?>
            </select>
				  </div>
				</div>

				<div class="form-group">
				  	<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<input type="submit" class="btn btn-success" name="submit" value="simpan data">
						</div>
				</div>

				<?=form_close()?>
			</div>

	  	<div class="modal-footer">
	    	<button type="button" class="btn btn-success pull-right" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
	  	</div>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Modal Tambah -->


<!-- Modal Delete -->
<?php $i=1; foreach ($tbl_rekam_medis as $rekam_medis) { ?>
<div class="modal modal-danger fade" id="delete<?php echo $rekam_medis->id?>">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $juduldelete ?></h4>
          </div>
          <div class="modal-body">
            <p>Yakin ingin menghapus data ini ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <a href="<?php echo base_url('RekamMedis/delete/'.$rekam_medis->id)?>"class="btn btn-outline pull-right">
              <i class="fa fa-trash-o"></i> Ya, Hapus Data ini
            </a>
            <button type="button" class="btn btn-outline pull-right" data-dismiss="modal"><i class="fa fa-backward"></i> Tidak, Batalkan</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php $i++; } ?>
<!-- End Modal Delete -->