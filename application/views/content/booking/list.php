<?php
// Notifikasi
if($this->session->flashdata('success'))
{
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('success');
	echo '</div>';
}

if($this->session->flashdata('danger'))
{
	echo '<div class="alert alert-danger">';
	echo $this->session->flashdata('danger');
	echo '</div>';
}

if($this->session->flashdata('warning'))
{
	echo '<div class="alert alert-warning">';
	echo $this->session->flashdata('warning');
	echo '</div>';
}

// Error warning
echo validation_errors('<div class="alert alert-warning">','</div>');

// Include tambah
?>

<div class="row">
	<div class="col-sm-12 text-right">
		<button type="button" title="Tambah" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah data baru</button>
	</div>
</div>

<br><hr>

<div class="box-body table-responsive no-padding">
<table id="example1" class="table table-hover table-bordered">
<thead>
<tr>
<th width="5%"><center>No</th></center>
<th><center>Nama Pasien</th></center>
<th><center>Tanggal Booking</th></center>
<th style="width: 15%;"><center>Action</th></center>
</tr>
</thead>
<tbody>

<?php $i=1; foreach ($tbl_booking as $booking) { ?>
<tr>
<td><center><?php echo $i?></td></center>
<td><?php echo strtoupper($booking->nama) ?></td>
<td><center><?php echo date('d M Y H:i', strtotime($booking->booking_date)) ?></center></td>
<td><center>
    <button type="button" title="Hapus" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete<?=$booking->id ?>"><i class="fa fa-trash"></i> Hapus</button>
</td></center>
</tr>
<?php $i++; } ?>

</tbody>
</table>


<!-- Modal Tambah -->
<div class="modal modal-default fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $juduladd ?></h4>
      </div>

<div class="modal-body">
	<?php
	// attribut
	$attribut='class="form-horizontal"';

	// Form open
	echo form_open(base_url('Booking/tambah'),$attribut);
	?>
	<div class="row">
		<div class="col-sm-12 text-right" style="margin-bottom: 10px;">
			<a href="<?php echo base_url('master/Pelanggan') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Pasien Baru</a>
		</div>
	</div>

	<div class="form-group">
	    <label class="col-sm-3 control-label">Nama Pasien</label>
	  <div class="col-sm-9">
            <select name="id_pelanggan" class="form-control select2" style="width: 100%; height: 35px;">
              <option value="">- Pilih -</option>
              <?php foreach ($tbl_pelanggan as $value) { ?>
              <option value="<?php echo $value->id ?>">
              <?php echo $value->nama ?> - <?php echo $value->no_hp ?>
              </option>
              <?php } ?>
            </select>
	  </div>
	</div>

	<div class="form-group">
	    <label class="col-sm-3 control-label">Tanggal Booking</label>
	  <div class="col-sm-9">
	    <input type="datetime-local" name="booking_date" class="form-control" value="<?php echo set_value('booking_date')?>" required>
	  </div>
	</div>

	<div class="form-group">
	  	<label class="col-sm-3 control-label"></label>
			<div class="col-sm-9">
				<input type="submit" class="btn btn-success" name="submit" value="simpan data">
			</div>
	</div>

	<?php 
	// Form close
	echo form_close();
	?>
</div>

	  <div class="modal-footer">
	    <button type="button" class="btn btn-success pull-right" data-dismiss="modal">
	      <i class="fa fa-times"></i> Close</button>
	  </div>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Modal Tambah -->



<?php $i=1; foreach ($tbl_booking as $booking) { ?>
<div class="modal modal-danger fade" id="delete<?php echo $booking->id?>">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $juduldelete ?></h4>
          </div>
          <div class="modal-body">
            <p>Yakin ingin menghapus data ini ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <a href="<?php echo base_url('Booking/delete/'.$booking->id)?>"class="btn btn-outline pull-right">
              <i class="fa fa-trash-o"></i> Ya, Hapus Data ini
            </a>
            <button type="button" class="btn btn-outline pull-right" data-dismiss="modal"><i class="fa fa-backward"></i> Tidak, Batalkan</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php $i++; } ?>
<!-- End Modal Delete -->