<style>
    table {
        border-collapse: collapse;
    }
    table, th, td {
        border: 1px solid black; width: 100px;
    }
    th, td {
        padding: 10px;
    }
    th {
        color: black;
    }

    .kanan{
      text-align: right; position: absolute;
    }
</style>

<table border="0px">
  <tr>
    <th><img src="assets/Admin/images/logo.jpeg" height="80px"></th>
  </tr>
</table>

<div class="kanan"  style="margin-top: 5%;">
<pre><h5 style="font-weight: none; font-size: 16px; font-family: Tahoma;">
Bekasi, <?=date('d F Y')?>

Puteri Beauty Care & Aesthetics
Jl. Perjuangan No.62 RT.004/RW.008,
Marga Mulya, Kec. Bekasi Utara.
Kota Bekasi, Jawa Barat
</h5> 
</pre>
</div>
<hr>
  <table>
    <tr style="text-align: center; font-family: Tahoma;">
      <th style="width: 5%;">No</th>
      <th style="width: 300px;">Nama Pelanggan</th>
      <th style="width: 100px;">Usia</th>
      <th style="width: 200px;">No. Handphone</th>
    </tr>
 
    <?php $no = 1; foreach ($tbl_pelanggan as $value): ?>
    <tr style="font-family: Tahoma;">
      <td style="width: 5%; text-align: center;"><?php echo $no++ ?></td>
      <td style="width: 300px;"><?php echo $value->nama ?></td>
      <td style="width: 100px; text-align: center; "><?php echo convertBirtdateToAge($value->tanggal_lahir) ?></td>
      <td style="width: 200px; text-align: center; "><?php echo $value->no_hp ?></td>
    </tr>
    <?php endforeach ?>
</table>

<table border="0px">
  <tr style="font-family: Tahoma; text-align: right;">
    <th style="padding-right: 20px;"><h5>Bekasi, <?= date('d F Y') ?></h5></th>
  </tr>

  <tr style="font-family: Tahoma;">
    <th width="920"><h5>( <?php echo $user ?> )</h5></th>
  </tr>
</table>