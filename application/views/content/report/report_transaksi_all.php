<style>
    table {
        border-collapse: collapse;
    }
    table, th, td {
        border: 1px solid black; width: 100px;
    }
    th, td {
        padding: 10px;
    }
    th {
        color: black;
    }

    .kanan{
      text-align: right; position: absolute;
    }
</style>

<table border="0px">
  <tr>
    <th><img src="assets/Admin/images/logo.jpeg" height="80px"></th>
  </tr>
</table>

<div class="kanan"  style="margin-top: 5%;">
<pre><h5 style="font-weight: none; font-size: 16px; font-family: Tahoma;">Bekasi, <?=date('d F Y')?><br/>
Puteri Beauty Care & Aesthetics
Jl. Perjuangan No.62 RT.004/RW.008,
Marga Mulya, Kec. Bekasi Utara.
Kota Bekasi, Jawa Barat
</h5></pre></div>
<hr><table>
  <tr style="text-align: center; font-family: Tahoma;">
    <th width="5%"><center>No</center></th>
    <th style="width: 150px;">No Faktur</th>
    <th style="width: 200px;">Tanggal Transaksi</th>
    <th style="width: 150px;">Jumlah Pembayaran</th>
    <th style="width: 100px;">Metode Pembayaran</th>
  </tr>
      
<?php $total = 0; $no = 1; foreach ($tbl_transaksi as $value): ?>
<?php 
  $data_metode = $value->metode_pembayaran;
  $metode = "-";
  if ($data_metode == 'P1') {
    $metode = 'Cash';
  }else if ($data_metode == 'P2') {
    $metode = 'Debit / Kredit';
  }
?>

  <tr style="text-align: center; font-family: Tahoma;">
    <td style="width: 5%;"><?php echo $no++ ?></td>
    <td style="width: 150px;"><?php echo $value->no_faktur ?></td>
    <td style="width: 200px;"><?php echo date('d F Y',strtotime($value->created_at)) ?></td>
    <td style="width: 150px;"><?php echo "Rp " . number_format($value->jumlah_pembayaran,2,',','.') ?></td>
    <td style="width: 100px;"><?php echo $metode ?></td>
  </tr>
  <?php $total += $value->jumlah_pembayaran ?>
  <?php endforeach ?>

  <tr style="font-family: Tahoma;">
      <td colspan="4" style="text-align: right;">Jumlah Pendapatan</td>
      <td style="text-align: center;"><?php echo "Rp " . number_format($total,0,',','.') ?></td>
    </tr>
</table>

<table border="0px">
  <tr style="font-family: Tahoma; text-align: right;">
    <th style="padding-right: 20px;"><h5>Bekasi, <?= date('d F Y') ?></h5></th>
  </tr>

  <tr style="font-family: Tahoma;">
    <th width="920"><h5>( <?php echo $user ?> )</h5></th>
  </tr>
</table>