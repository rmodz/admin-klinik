<style>
    table {
        border-collapse: collapse;
    }
    table, th, td {
        border: 1px solid black; width: 100px;
    }
    th, td {
        padding: 10px;
    }
    th {
        color: black;
    }

    .kanan{
      text-align: right; position: absolute;
    }
</style>

<table border="0px">
  <tr>
    <th><img src="assets/Admin/images/logo.jpeg" height="80px"></th>
  </tr>
</table>

<div class="kanan"  style="margin-top: 5%;">
<pre><h5 style="font-weight: none; font-size: 16px; font-family: Tahoma;">
Bekasi, <?= date('d F Y') ?><br/>
Puteri Beauty Care & Aesthetics
Jl. Perjuangan No.62 RT.004/RW.008,
Marga Mulya, Kec. Bekasi Utara.
Kota Bekasi, Jawa Barat
</h5> 
</pre>
</div>

<hr>

<h5 style="font-family: Tahoma;">No. Faktur : <?php echo $tbl_transaksi_join->no_faktur ?></h5>
<br>


  <table>
    <tr style="text-align: center; font-family: Tahoma;">
      <th width="5">No</th>
      <th width="250">Nama Barang</th>
      <th>Jumlah</th>
      <th>Harga</th>
    </tr>
    
  <?php $no = 1; foreach ($tbl_transaksi_join->detail as $value): ?>
    <tr style="font-family: Tahoma;">
      <td style="text-align: center;"><?php echo $no++ ?></td>
      <td><?php echo $value->nama ?></td>
      <td style="text-align: center;"><?php echo $value->jumlah_barang ?></td>
      <td><?php echo "Rp " . number_format($value->harga,0,',','.') ?></td>
    </tr>
  <?php endforeach ?>

    <tr style="font-family: Tahoma;">
      <td colspan="2"></td>
      <td>Subtotal</td>
      <td><?php echo "Rp " . number_format($tbl_transaksi_join->jumlah_pembayaran - $tbl_transaksi_join->kembali,0,',','.') ?></td>
    </tr>

    <tr style="font-family: Tahoma;">
      <td colspan="2"></td>
      <td>Dibayar</td>
      <td><?php echo "Rp " . number_format($tbl_transaksi_join->jumlah_pembayaran,0,',','.') ?></td>
    </tr>

    <tr style="font-family: Tahoma;">
      <td colspan="2"></td>
      <td>Kembali</td>
      <td><?php echo "Rp " . number_format($tbl_transaksi_join->kembali,0,',','.') ?></td>
    </tr>
</table>


<table border="0px">
  <tr style="font-family: Tahoma; text-align: right;">
    <th style="padding-right: 50px;"><h5>Bekasi, <?= date('d F Y') ?></h5></th>
  </tr>

  <tr style="font-family: Tahoma;">
    <th width="980"><h5>( <?php echo $user ?> )</h5></th>
  </tr>
</table>