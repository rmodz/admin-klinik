<?php
// Notifikasi
if($this->session->flashdata('success'))
{
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('success');
	echo '</div>';
}

if($this->session->flashdata('danger'))
{
	echo '<div class="alert alert-danger">';
	echo $this->session->flashdata('danger');
	echo '</div>';
}

if($this->session->flashdata('warning'))
{
	echo '<div class="alert alert-warning">';
	echo $this->session->flashdata('warning');
	echo '</div>';
}

// Error warning
echo validation_errors('<div class="alert alert-warning">','</div>');

// Include tambah
?>

<div class="row">
	<div class="col-sm-12 text-right">
		<button type="button" title="Tambah" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah Transaksi</button>
	</div>
</div>

<br><hr>

<div class="box-body table-responsive no-padding">
<table id="example1" class="table table-hover table-bordered">
<thead>
<tr>
<th width="5%"><center>No</th></center>
<th><center>No faktur</center></th>
<th><center>Total pembayaran</center></th>
<th><center>Metode pembayaran</center></th>
<th><center>Status</center></th>
<th style="width: 15%;"><center>Action</center></th>
</tr>
</thead>
<tbody>

<?php $i=1; foreach ($tbl_transaksi as $transaksi) { ?>

<?php 
	$data_metode = $transaksi->metode_pembayaran;
	$metode = "-";
	if ($data_metode == 'P1') {
		$metode = 'Cash';
	}else if ($data_metode == 'P2') {
		$metode = 'Debit / Cash';
	}
?>

<?php 
	$data_status = $transaksi->selesai;
	$status = "";
	if ($data_status == '1') {
		$status = 'Selesai';
	} else {
		$status = 'Belum Selesai';
	}
?>

<tr>
<td class="text-center"><center><?php echo $i?></td></center>
<td class="text-center"><?php echo strtoupper($transaksi->no_faktur) ?></td>
<td class="text-right"><?php echo "Rp " . number_format($transaksi->total_harga,0,',','.'); ?></td>
<td class="text-center"><?php echo $metode ?></td>
<td class="text-center"><?php echo $status ?></td>

<td class="text-center">
	<?php if ($transaksi->selesai != "0"): ?>
    <a href="<?php echo base_url('Transaksi/print/'.$transaksi->no_faktur) ?>" target="_blank" class="btn btn-info btn-sm">Cetak Struck Transaksi</a>
    <?php else: ?>
	<a href="<?=site_url('Transaksi	/edit/'.$transaksi->id)?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Edit</a>
    <button type="button" title="Hapus" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete<?=$transaksi->id ?>"> Hapus</button>
  <?php endif ?>
</td>
</tr>
<?php $i++; } ?>

</tbody>
</table>

<!-- Modal Tambah -->
<div class="modal modal-default fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $juduladd ?></h4>
      </div>

      		<input type="hidden" name="no_faktur" class="form-control" value="<?php echo $notis;?>">

			<div class="modal-body">
				<?=form_open(base_url('Transaksi/tambah'), 'class="form-horizontal"')?>
				<div class="form-group">
		    		<label class="col-sm-3 control-label">Nama Pasien</label>
		  			<div class="col-sm-9">
			            <select name="id_rekam_medis" class="form-control select2" style="width: 100%; height: 35px;" required>
			              <option value="">- Pilih -</option>
			              <?php foreach ($tbl_rekam_medis as $value) { ?>
			              <option value="<?php echo $value->id ?>">
			              <?php echo $value->nama_pelanggan ?>
			              </option>
			              <?php } ?>
			            </select>
					</div>
				</div>
				

				<div class="form-group">
				  	<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<input type="submit" class="btn btn-success" name="submit" value="simpan data">
						</div>
				</div>
				<?=form_close()?>
			</div>

	  	<div class="modal-footer">
	    	<button type="button" class="btn btn-success pull-right" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
	  	</div>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Modal Tambah -->

<!-- Modal Delete -->
<?php $i=1; foreach ($tbl_transaksi as $transaksi) { ?>
<div class="modal modal-danger fade" id="delete<?php echo $transaksi->id?>">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $juduldelete ?></h4>
          </div>
          <div class="modal-body">
            <p>Yakin ingin menghapus data ini ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <a href="<?php echo base_url('Transaksi/delete/'.$transaksi->id)?>"class="btn btn-outline pull-right">
              <i class="fa fa-trash-o"></i> Ya, Hapus Data ini
            </a>
            <button type="button" class="btn btn-outline pull-right" data-dismiss="modal"><i class="fa fa-backward"></i> Tidak, Batalkan</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php $i++; } ?>
<!-- End Modal Delete -->