<form action="<?php echo base_url('report/Report_transaksi') ?>" method="post">
  <div class="row">
    <div class="col-sm-6">
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">
            <td>Dari</td>
            <input name="start" type="date" class="form-control" value="<?=$start_date?>">
        </span>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">
          <td>Sampai</td>
          <input name="end" type="date" class="form-control" value="<?=$end_date?>">
        </span>
      </div>
    </div>
    <div class="col-sm-12 text-center" style="margin-top: 30px; margin-bottom: 20px;">
      <button type="submit" class="btn btn-info btn-sm" style="border-radius: 5px; font-size: 16px;"><i class="fa fa-search"></i> TARIK DATA REPORT</button>
    </div>
  </div>
</form>
<br>

<?php if (!empty($start_date) && !empty($end_date)): ?>
<div class="row">
  <div class="input-group col-md-2 col-md-offset-10">
    <a href="<?php echo base_url('report/Report_transaksi/print/'.$start_date.'/'.$end_date)?>" target="_blank" class="btn btn-flat btn-success btn-sm"> <i class="fa fa-book"></i> CETAK PDF</a>
  </div>
</div>
<?php endif ?>

<br>
<!-- LIST -->
<div class="box-body table-responsive no-padding">
<table id="example1" class="table table-hover table-bordered">
<thead>
<tr>
<th width="5%"><center>No</th></center>
<th><center>No faktur</center></th>
<th><center>Total pembayaran</center></th>
<th><center>Metode pembayaran</center></th>
<th><center>Tanggal Transaksi</center></th>
</tr>
</thead>
<tbody>

<?php $i=1; foreach ($tbl_transaksi as $transaksi) { ?>

<?php 
  $data_metode = $transaksi->metode_pembayaran;
  $metode = "-";
  if ($data_metode == 'P1') {
    $metode = 'Cash';
  }else if ($data_metode == 'P2') {
    $metode = 'Debit / Kredit';
  }
?>

<?php 
  $data_status = $transaksi->selesai;
  $status = "";
  if ($data_status == '1') {
    $status = 'Selesai';
  } else {
    $status = 'Belum Selesai';
  }
?>

<tr>
<td class="text-center"><center><?php echo $i?></td></center>
<td class="text-center"><?php echo strtoupper($transaksi->no_faktur) ?></td>
<td class="text-right"><?php echo "Rp " . number_format($transaksi->total_harga,0,',','.'); ?></td>
<td class="text-center"><?php echo $metode ?></td>
<td class="text-center"><?php echo $transaksi->created_at ?></td>
</tr>
<?php $i++; } ?>

</tbody>
</table>

<script type="text/javascript">
    $(function() {
        // DATES
        var $input = $('#from').pickadate();
        var picker = $input.pickadate('picker');
        picker.set('select', '<?= $from ?>', {
            format: 'yyyy-mm-dd'
        })
        var $input2 = $('#until').pickadate();
        var picker2 = $input2.pickadate('picker');
        picker2.set('select', '<?= $until ?>', {
            format: 'yyyy-mm-dd'
        })
    })
    function searchData() {
        var from = convertToSqlDate($("#from").val());
        var until = convertToSqlDate($("#until").val());
        console.log(from);
        console.log(until);
        window.location.href = "<?= site_url('Pelanggan/reaktivasi_pelanggan?from="+from+"&until="+until+"') ?>";
    }
</script>