<?php 
// Notifikasi
if($this->session->flashdata('success'))
{
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('success');
	echo '</div>';
}

if($this->session->flashdata('danger'))
{
	echo '<div class="alert alert-danger">';
	echo $this->session->flashdata('danger');
	echo '</div>';
}

if($this->session->flashdata('warning'))
{
	echo '<div class="alert alert-warning">';
	echo $this->session->flashdata('warning');
	echo '</div>';
}
// Error warning
echo validation_errors('<div class="alert alert-warning">','</div>');

// Atribut
// $attribut = 'class="alert alert-info"';
$attribut = '';
// Form open
echo form_open( base_url('Transaksi/edit/'.$tbl_transaksi->id),$attribut);

// Csrf protection = Perlindungan terhadap form dari serangan hacker
?>

<div class="box box-solid box-info">
	<div class="box-header with-border">
		<div class="row">
			<div class="col col-sm-6">
	  		<h3 class="box-title"><b>Data Pasien</b></h3>
			</div>
			<div class="col col-sm-6 text-right">
	  		<!-- <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#tambah-home-care"> <KBD></KBD>embali</button> -->
	  		<input type="hidden" name="id" value="<?=$tbl_transaksi->id?>">
	  		<button class="btn btn-success btn-sm" type="submit"> Bayar</button>
			</div>
		</div>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col col-sm-6">
				<div class="form-group">
				    <label for="inputAge">Nomor Faktur</label>
			   		<input type="text" class="form-control" placeholder="Nama Pasien" readonly value="<?=$tbl_transaksi->no_faktur?>">
				</div>

				<div class="form-group">
				    <label for="inputAge">Nama Pasien</label>
			   		<input type="text" class="form-control" placeholder="Nama Pasien" readonly value="<?=$tbl_transaksi->nama_pelanggan?>">
				</div>

				<div class="form-group">
				    <label for="inputAge">Umur</label>
			   		<input type="text" class="form-control" placeholder="Umur" readonly value="<?=convertBirtdateToAge($tbl_transaksi->tanggal_lahir_pelanggan)?>">
				</div>
			</div>

			<div class="col col-sm-6">
				<div class="form-group">
				    <label for="inputAge">Jumlah Item</label>
			   		<input type="text" class="form-control" placeholder="Jumlah Item" readonly value="<?=$total_item?>">
				</div>

				<div class="form-group">
				    <label for="inputAge">Total Harga</label>
			   		<input type="text" class="form-control" placeholder="Total Harga" readonly name="total_harga" value="<?=number_format($total_harga,0,',','.')?>">
				</div>

				<div class="row">
					<div class="col col-sm-4">
						<div class="form-group">
					    <label for="inputAge">Metode Pembayaran</label>
				   		<!-- <div class="radio">
		            <label> <input type="radio" name="metode_pembayaran" value="cash"> Cash</label>
		            <label> <input type="radio" name="metode_pembayaran" value="debit"> Debit/Kredit</label>
		        	</div> -->
		        	<select id="method" name="metode_pembayaran" class="form-control" required>
		        		<option value="cash">Cash</option>
		        		<option value="debit">Debit/Credit</option>
		        	</select>
						</div>
					</div>
					<div class="col col-sm-8">
						<div id="card" class="form-group" style="display: none;">
					    <label for="inputAge">Nomor Kartu</label>
				   		<input id="debit" type="text" class="form-control input-mask" name="nomor_kartu" data-inputmask="'mask': '9999 9999 9999 9999'">
						</div>

						<div id="cash" class="form-group">
					    <label for="inputAge">Jumlah Bayar</label>
				   		<input id="rupiah" type="text" class="form-control input-mask" name="jumlah_bayar" required="true">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?=form_close()?>

<div class="box box-solid box-info">
	<div class="box-header with-border">
		<div class="row">
			<div class="col col-sm-6">
	  		<h3 class="box-title"><b>Data Transaksi</b></h3>
			</div>
		</div>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col col-sm-12 text-right">
	  		<button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#tambah-home-care"> Tambah Data</button>
			</div>
		</div>
		<div class="row">
				<div class="box-body table-responsive">
					<table class="table table-hover table-bordered dtable">
						<thead>
							<tr>
								<th width="5%" class="text-center" >No</th>
								<th>Nama</th>
								<th>Tipe</th>
								<th>Jumlah</th>
								<th>Harga</th>
								<th style="width: 15%;" class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($tbl_transaksi->detail as $key => $value): ?>
								<tr>
									<th class="text-center"><?=$key+1?></th>
									<th><?=$value->nama?></th>
									<th><?=$value->type == "T1" ? "Treatment":"Produk"?></th>
									<th><?=$value->jumlah_barang?></th>
									<th><?=number_format($value->harga,0,',','.')?></th>
									<th class="text-center">
										<a class="btn btn-danger btn-xs" href="<?=site_url('Transaksi/delete_detail/'.$value->id.'/'.$value->id_transaksi)?>"><i class="fa fa-trash"></i></a>
									</th>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
		</div>
	</div>
</div>

<div class="modal modal-default fade" id="tambah-home-care">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Home Care</h4>
      </div>
      		
			<div class="modal-body">
				<?=form_open(base_url('Transaksi/tambah_detail'), 'class="form-horizontal"')?>

	      		<input type="hidden" name="id_transaksi" value="<?php echo $tbl_transaksi->id ?>">

				<div class="form-group">
	    		<label class="col-sm-3 control-label">Produk</label>
	  				<div class="col-sm-9" style="padding-bottom: 10px;">
		            <select name="id_ref" class="form-control select2" style="width: 100%; height: 35px;" required>
		            	  <option value="">- Pilih -</option>
			              <?php foreach ($tbl_produk as $value) { ?>
			              <option value="<?php echo $value->id ?>">
			              <?php echo $value->nama ?>
			              </option>
			              <?php } ?>
		            </select>
				  	</div>
				</div>

				<div class="form-group">
	    		<label class="col-sm-3 control-label">Jumlah</label>
		  			<div class="col-sm-9" style="padding-bottom: 10px;">
            			<input type="number" class="form-control" name="jumlah_barang" value="1" required onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
				  	</div>
				</div>

				<div class="form-group">
				  	<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<input type="submit" class="btn btn-success" name="submit" value="simpan data">
						</div>
				</div>

				<?=form_close()?>
			</div>

	  	<div class="modal-footer">
	    	<button type="button" class="btn btn-success pull-right" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
	  	</div>

    </div>
  </div>
</div>

<script type="text/javascript">
	$(function() {
		$('#method').change(function(event) {
			if ($(this).val() == 'cash') {
				$('#rupiah').val("").prop('required', 'true');
				$('#debit').val("").removeAttr('required');
				$('#card').hide();
				$('#cash').show();
			} else {
				$('#debit').val("").prop('required', 'true');
				$('#rupiah').val("").removeAttr('required');
				$('#cash').hide();
				$('#card').show();
			}
		});

		$("#rupiah").keyup(function(event) {
			$(this).val(formatRupiah(this.value, "Rp. "));
		});
	});

	/* Fungsi formatRupiah */
function formatRupiah(angka, prefix) {
  var number_string = angka.replace(/[^,\d]/g, "").toString(),
    split = number_string.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if (ribuan) {
    separator = sisa ? "." : "";
    rupiah += separator + ribuan.join(".");
  }

  rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
  return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
}
</script>
