<form action="" method="post">
  <div class="row">
    <div class="col-sm-6">
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">
          <td>Dari</td>
            <input name="start" type="date" class="form-control" value="<?=$start_date?>">
        </span>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">
          <td>Sampai</td>
            <input name="end" type="date" class="form-control" value="<?=$end_date?>">
        </span>
      </div>
    </div>
    <div class="col-sm-12 text-center" style="margin-top: 30px; margin-bottom: 20px;">
      <button type="submit" class="btn btn-info btn-sm" style="border-radius: 5px; font-size: 16px;"><i class="fa fa-search"></i> TARIK DATA REPORT</button>
    </div>
  </div>
</form>
<br>

<?php if (!empty($start_date) && !empty($end_date)): ?>
<div class="row">
  <div class="input-group col-md-2 col-md-offset-10">
    <a href="<?php echo base_url('report/Report_treatment/print/'.$start_date.'/'.$end_date)?>" target="_blank" class="btn btn-flat btn-success btn-sm"> <i class="fa fa-book"> </i> CETAK PDF</a>
  </div>
</div>
<?php endif ?>

<br>
<!-- LIST -->
<div class="box-body table-responsive no-padding">
  <table id="example2" class="table table-hover table-bordered">
  <thead>
  <tr>
  <th width="5%"><center>No</center></th>
  <th><center>Nama Treatment</center></th>
  <th><center>Harga Treatment</center></th>
  </tr>
  </thead>
  <tbody>

  <?php 
  $i=1;
  foreach ($tbl_treatment as $treatment) {
   ?>
  <tr>
  <td><center><?php echo $i?></td></center>
  <td><?php echo strtoupper($treatment->nama) ?></td>
  <td style="text-align: right;"><?php echo "Rp " . number_format($treatment->harga,2,',','.'); ?></td>
  </tr>
  <?php $i++; } ?>
  </tbody>
  </table>