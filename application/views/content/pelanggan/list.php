<?php
// Notifikasi
if($this->session->flashdata('success'))
{
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('success');
	echo '</div>';
}

if($this->session->flashdata('danger'))
{
	echo '<div class="alert alert-danger">';
	echo $this->session->flashdata('danger');
	echo '</div>';
}

if($this->session->flashdata('warning'))
{
	echo '<div class="alert alert-warning">';
	echo $this->session->flashdata('warning');
	echo '</div>';
}

// Error warning
echo validation_errors('<div class="alert alert-warning">','</div>');

// Include tambah
?>

<div class="row">
	<div class="col-sm-12 text-right">
		<button type="button" title="Tambah" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah data baru</button>
	</div>
</div>

<br><hr>

<div class="box-body table-responsive no-padding">
<table id="example1" class="table table-hover table-bordered">
<thead>
<tr>
<th width="5%"><center>No</th></center>
<th><center>Nama</th></center>
<th><center>Tangggal Lahir</th></center>
<th><center>Alamat</th></center>
<th><center>No. Handphone</th></center>
<th><center>Pekerjaan</th></center>
<th style="width: 25%;"><center>Action</th></center>
</tr>
</thead>
<tbody>

<?php $i=1; foreach ($tbl_pelanggan as $pelanggan) { ?>
<tr>
<td><center><?php echo $i?></td></center>
<td><?php echo $pelanggan->nama ?></td>
<td><?php echo $pelanggan->tanggal_lahir ?></td>
<td><?php echo $pelanggan->alamat ?></td>
<td><?php echo $pelanggan->no_hp ?></td>
<td><?php echo $pelanggan->pekerjaan ?></td>
<td><center>
	<button type="button" title="Edit" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#edit<?=$pelanggan->id ?>"><i class="fa fa-edit"></i> Edit</button>
    <button type="button" title="Hapus" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete<?=$pelanggan->id?>"><i class="fa fa-trash"></i> Hapus</button>
     <a href="<?php echo base_url('master/Riwayat_pelanggan/list/'.$pelanggan->id)?> " title="Riwayat" class="btn btn-primary btn-sm"><i class="fa fa-user"></i> Riwayat</a>
</td></center>
</tr>
<?php $i++; } ?>

</tbody>
</table>

<!-- Modal Tambahh -->
<div class="modal modal-default fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $juduladd ?></h4>
      </div>

<div class="modal-body">
	<?php
	// attribut
	$attribut='class="form-horizontal"';

	// Form open
	echo form_open(base_url('master/Pelanggan/tambah'),$attribut);
	?>
	<div class="form-group">
	    <label class="col-sm-3 control-label">Nama Pelanggan</label>
	  <div class="col-sm-9">
	    <input type="text" name="nama" class="form-control" placeholder="Masukan Nama Pelanggan" value="<?php echo set_value('nama')?>" required>
	  </div>
	</div>

	<div class="form-group">
	    <label class="col-sm-3 control-label">Tanggal Lahir</label>
	  <div class="col-sm-9">
	    <input type="date" name="tanggal_lahir" class="form-control" value="<?php echo set_value('tanggal_lahir')?>" required>
	  </div>
	</div>

	<div class="form-group">
	       <label class="col-sm-3 control-label">Alamat</label>
	  <div class="col-sm-9">
	  	<textarea class="form-control" placeholder="Masukkan Alamat" name="alamat" value="<?php echo set_value('password')?>" required></textarea>
	  </div>
	</div>

	<div class="form-group">
	    <label class="col-sm-3 control-label">No Handphone</label>
	  <div class="col-sm-9">
	    <input type="text" name="no_hp" class="form-control" placeholder="Masukan No Handphone" value="<?php echo set_value('no_hp')?>" required>
	  </div>
	</div>

	<div class="form-group">
	    <label class="col-sm-3 control-label">Pekerjaan</label>
	  <div class="col-sm-9">
	    <input type="text" name="pekerjaan" class="form-control" placeholder="Masukan Pekerjaan" value="<?php echo set_value('pekerjaan')?>" required>
	  </div>
	</div>

	<div class="form-group">
	  	<label class="col-sm-3 control-label"></label>
			<div class="col-sm-9">
				<input type="submit" class="btn btn-success" name="submit" value="simpan data">
			</div>
	</div>

	<?php 
	// Form close
	echo form_close();
	?>
</div>

	  <div class="modal-footer">
	    <button type="button" class="btn btn-success pull-right" data-dismiss="modal">
	      <i class="fa fa-times"></i> Close</button>
	  </div>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Modal Tambah -->


<!-- Modal Edit -->
<?php $i=1; foreach ($tbl_pelanggan as $pelanggan) { ?>
<div class="modal modal-default fade" id="edit<?php echo $pelanggan->id ?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $juduledit ?></h4>
      </div>

<div class="modal-body">
	<?php
	// attribut
	$attribut='class="form-horizontal"';

	// Form open
	echo form_open(base_url('master/Pelanggan/edit/'.$pelanggan->id),$attribut);
	?>
	
	<div class="form-group">
	    <label class="col-sm-3 control-label">Nama Pelanggan</label>
	  <div class="col-sm-9">
	    <input type="text" name="nama" class="form-control" placeholder="Masukan Nama Pelanggan" value="<?php echo $pelanggan->nama ?>" required>
	  </div>
	</div>

	<div class="form-group">
	    <label class="col-sm-3 control-label">Tanggal Lahir</label>
	  <div class="col-sm-9">
	    <input type="date" name="tanggal_lahir" class="form-control" value="<?php echo $pelanggan->tanggal_lahir ?>" required>
	  </div>
	</div>

	<div class="form-group">
	       <label class="col-sm-3 control-label">Alamat</label>
	  <div class="col-sm-9">
	    <input type="text" name="alamat" class="form-control" placeholder="Masukan No Handphone" value="<?php echo $pelanggan->alamat ?>" required>
	  </div>
	</div>

	<div class="form-group">
	    <label class="col-sm-3 control-label">No Handphone</label>
	  <div class="col-sm-9">
	    <input type="text" name="no_hp" class="form-control" placeholder="Masukan No Handphone" value="<?php echo $pelanggan->no_hp ?>" required>
	  </div>
	</div>

	<div class="form-group">
	    <label class="col-sm-3 control-label">Pekerjaan</label>
	  <div class="col-sm-9">
	    <input type="text" name="pekerjaan" class="form-control" placeholder="Masukan No Handphone" value="<?php echo $pelanggan->pekerjaan ?>" required>
	  </div>
	</div>

	<div class="form-group">
	  	<label class="col-sm-3 control-label"></label>
			<div class="col-sm-9">
				<input type="submit" class="btn btn-success" name="submit" value="simpan data">
			</div>
	</div>

	<?php 
	// Form close
	echo form_close();
	?>
</div>

	  <div class="modal-footer">
	    <button type="button" class="btn btn-success pull-right" data-dismiss="modal">
	      <i class="fa fa-times"></i> Close</button>
	  </div>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php $i++; } ?>
<!-- End Modal Edit -->

<!-- Modal Delete -->
<?php $i=1; foreach ($tbl_pelanggan as $pelanggan) { ?>
<div class="modal modal-danger fade" id="delete<?php echo $pelanggan->id?>">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $juduldelete ?></h4>
          </div>
          <div class="modal-body">
            <p>Yakin ingin menghapus data ini ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <a href="<?php echo base_url('master/Pelanggan/delete/'.$pelanggan->id)?>"class="btn btn-outline pull-right">
              <i class="fa fa-trash-o"></i> Ya, Hapus Data ini
            </a>
            <button type="button" class="btn btn-outline pull-right" data-dismiss="modal"><i class="fa fa-backward"></i> Tidak, Batalkan</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php $i++; } ?>
<!-- End Modal Delete -->